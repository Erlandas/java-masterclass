package com.erlandas;

import java.util.ArrayList;

public class Account {


    /*
    We cannot make those fields PUBLIc as it has potentiel for fraud or error
    while making them public we can alter data directly from the main class
    thats wrong in good programming practice
     */
    public String accountName;
    public int balance = 0;
    public ArrayList<Integer> transactions;

    public Account(String accountName) {
        this.accountName = accountName;
        this.transactions = new ArrayList<>();
    }

    public int getBalance() {
        return balance;
    }

    public void deposit(int amount) {
        if( amount > 0 ) {
            this.transactions.add(amount);
            this.balance += amount;
            System.out.println(amount + " deposited balance now " + this.balance);
        } else {
            System.out.println("Cannot deposit negative sums");
        }
    }

    public void withdraw(int amount) {
        int withdrawal = -amount;
        if(withdrawal < 0) {
            this.transactions.add(withdrawal);
            this.balance += withdrawal;
            System.out.println(amount + " withdrawn. Balance now " + this.balance);
        } else {
            System.out.println("Cannot withdrawn negative sums");
        }
    }

    public void calculateBalance() {
        this.balance = 0;
        for(int i: this.transactions) {
            this.balance += i;
        }

        System.out.println("Calculated balance is : " + this.balance);
    }
}
