package com.erlandas;

import java.util.ArrayList;

public class Branch {
    private String branchName;
    private ArrayList<Customer> customers;

    public Branch(String branchName) {
        this.branchName = branchName;
        this.customers = new ArrayList<Customer>();
    }

    public String getBranchName() {
        return branchName;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    public boolean newCustomer(String name, double initialAmount) {
        if(findCustomer(name) == null) {
            this.customers.add(new Customer(name,initialAmount));
            return true;
        }

        return false;
    }

    public boolean addTransactions(String name,double transactionAmount) {
        Customer existingCustomer = findCustomer(name);
        if(existingCustomer != null) {
            existingCustomer.addTransaction(transactionAmount);
            return true;
        }
        return false;
    }

    private Customer findCustomer(String name) {
        for (int i = 0 ; i < customers.size() ; i++) {
            Customer currentCustomer = this.customers.get(i);

            if(currentCustomer.getCustomerName().equals(name)) {
                return currentCustomer;
            }
        }
        return null;
    }
}
