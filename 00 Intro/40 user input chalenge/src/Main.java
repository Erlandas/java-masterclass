import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean hasNextInt;
        int currentNumber;
        int counter = 1;
        int sum = 0;

        while (counter <= 10) {

            System.out.print("Enter number #" + counter + " : ");
            hasNextInt = scanner.hasNextInt();

            if (hasNextInt) {
                currentNumber = scanner.nextInt();
                sum+=currentNumber;
                counter++;
            } else {
                System.out.println("Make sure you entered a number");
            }
            scanner.nextLine();
            System.out.println("\tCurrent sum " +sum);

        }

        System.out.println("The SUM is " + sum);

        scanner.close();
    }
}
