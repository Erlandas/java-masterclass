package com.erlandas;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

public class Main {

    public static void main(String[] args) {

//        DirectoryStream.Filter<Path> filter =
//                new DirectoryStream.Filter<Path>() {
//                    public boolean accept(Path path) throws IOException {
//                        return (Files.isRegularFile(path));
//                    }
//                };

        //LAMBDA

        DirectoryStream.Filter<Path> filter = p -> Files.isRegularFile(p);

        //--   Creating the path to the directory

//        Path directory = FileSystems.getDefault().getPath("Filetree\\Dir2");


        //********Using inbuilt separator
        //********Good practice not to hard code separator
        Path directory = FileSystems.getDefault().getPath("Filetree" + File.separator + "Dir2");

        //--    We creating directory stream in resources section
        //--    To get the contents in that folder                              // What type of file to print
        try(DirectoryStream<Path> contents = Files.newDirectoryStream(directory, filter)) {

            //--    Iterate through directories contents
            for (Path file: contents) {
                System.out.println(file.getFileName());
            }

            //--> | <-- inclusive OR use instead of making couple of catch blocks
        } catch (IOException | DirectoryIteratorException e) {
            System.out.println(e.getMessage());
        }

        String separator = File.separator;
        System.out.println(separator);
        separator = FileSystems.getDefault().getSeparator();
        System.out.println(separator);


        //**********Create temporary files

        try {
            //                                      Prefix(name) , suffix(extension)
            Path tempFile = Files.createTempFile("myapp" , ".appext");
            System.out.println("Temporary file path = " + tempFile.toAbsolutePath());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        //Shows file storage devices such as USB devices, HDD partitions etc...
        Iterable<FileStore> stores = FileSystems.getDefault().getFileStores();
        for (FileStore store : stores) {
            System.out.println("Volume name / drive letter = " + store);  //--    Prints drive name with letter
            System.out.println("Volume name = " + store.name());   //--    Prints only drive name
        }

        System.out.println("**********************************************");

        //************To print root directory available drives are printed
        Iterable<Path> rootPath = FileSystems.getDefault().getRootDirectories();
        for(Path path : rootPath) {
            System.out.println(path);
        }

    }

}

//https://docs.oracle.com/javase/8/docs/api/java/nio/file/FileSystem.html#getPathMatcher-java.lang.String-
