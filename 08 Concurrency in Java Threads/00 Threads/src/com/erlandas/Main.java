package com.erlandas;

import static com.erlandas.ThreadColor.ANSI_GREEN;
import static com.erlandas.ThreadColor.ANSI_PURPLE;

public class Main {

    public static void main(String[] args) {
        /*
        We cant guaranty the order in witch threads will be executed
         */

        System.out.println(ANSI_PURPLE +"Hello from the main thread");


        Thread anotherThread = new AnotherThread();
        //we cant start same thread multiple times only ONCE
        anotherThread.start();

        new Thread() {
            public void run() {
                System.out.println(ANSI_GREEN + "Hello from the anonymous class thread");
            }
        }.start();

        System.out.println(ANSI_PURPLE + "Hello again from the main thread");
    }
}
