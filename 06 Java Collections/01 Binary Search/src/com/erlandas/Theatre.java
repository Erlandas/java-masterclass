package com.erlandas;

import java.util.*;

public class Theatre {

    private final String theatreName;
    //W can  change List to type Collection to make it really generic
    //Advantage of doing that that enables us to use any of collection classes
    private List<Seat> seats = new ArrayList<>();

    public Theatre(String theatreName, int numRows, int seatsPerRow) {
        this.theatreName = theatreName;

        int lastRow = 'A' + (numRows-1);

        //Populate list with seat numbers
        for(char row = 'A' ; row <= lastRow; row++) {
            for(int seatNum = 1 ; seatNum <= seatsPerRow ; seatNum++) {
                Seat seat = new Seat(row + String.format("%02d",seatNum));
                seats.add(seat);
            }
        }
    }


    //***************************************************************************************************
    public String getTheatreName() {
        return theatreName;
    }


    //***************************************************************************************************
    //to reserve a seat
//    public boolean reserveSeat(String seatNumber) {
//        Seat requestedSeat = new Seat(seatNumber);
//
//        //use binary search from collections
//        int foundSeat = Collections.binarySearch(seats,requestedSeat,null);
//        if(foundSeat >= 0) {
//            return seats.get(foundSeat).reserve();
//        } else {
//            return false;
//        }
//    }
    //***************************************************************************************************
    //to reserve a seat example real binary search
    public boolean reserveSeat(String seatNumber) {
        int low = 0;
        int high = seats.size()-1;

        while (low <= high) {
            System.out.print(".");
            int mid = (low + high) / 2;
            Seat midVal = seats.get(mid);
            int cmp = midVal.getSeatNumber().compareTo(seatNumber);

            if (cmp < 0) {
                low = mid + 1;
            } else if (cmp > 0) {
                high = mid - 1;
            } else {
                return seats.get(mid).reserve();
            }
        }
        System.out.println("There is no seat " + seatNumber);
        return false;
    }



    //***************************************************************************************************
    //for testing
    public void getSeats() {
        for(Seat seat: seats) {
            System.out.println(seat.getSeatNumber());
        }
    }




    //***************************************************************************************************
    private class Seat implements Comparable<Seat> {

        private final String seatNumber;
        private boolean reserved = false;

        public Seat(String seatNumber) {
            this.seatNumber = seatNumber;
        }


        //***************************************************************************************************
        //Overide compareTo method
        @Override
        public int compareTo(Seat seat) {
            return this.seatNumber.compareToIgnoreCase(seat.getSeatNumber());
        }


        //***************************************************************************************************
        //To reserve a seat
        public boolean reserve() {
            if(!this.reserved) {
                this.reserved = true;
                System.out.println("Seat " +seatNumber+ " reserved");
                return true;
            } else {
                return false;
            }
        }

        //to cancel reserved seat
        public boolean cancel() {
            if(this.reserved) {
                this.reserved = false;
                System.out.println("Reservation of seat " +seatNumber+ " canceled");
                return true;
            } else {
                return false;
            }
        }


        //***************************************************************************************************
        //Getter for seat number
        public String getSeatNumber() {
            return seatNumber;
        }
    }
}
