package com.erlandas;


//WE cant directly instantiat abstract classes
public abstract class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }


    //abstract methods means other classes must implement those methods
    public abstract void eat();
    public abstract void breathe();

    public String getName() {
        return name;
    }
}
