package com.erlandas;

import java.util.Collection;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {

        FootballPlayer joe = new FootballPlayer("Joe");
        BaseballPlayer pat = new BaseballPlayer("Pat");
        SoccerPlayer beckham = new SoccerPlayer("Beckham");

        //team
        Team<FootballPlayer> DublinMoorans = new Team<>("Dublin Moorans");
        DublinMoorans.addPlayer(joe);
//        DublinMoorans.addPlayer(pat);
//        DublinMoorans.addPlayer(beckham);

        //team
        Team<BaseballPlayer> baseballTeam = new Team<>("Chicago cubs");
        baseballTeam.addPlayer(pat);

        //team
        Team<SoccerPlayer> brokenTeam = new Team<>("Broken Team");
        brokenTeam.addPlayer(beckham);

        //team
        Team<FootballPlayer> melbourne = new Team<>("Melbourne");
        FootballPlayer banks = new FootballPlayer("Gordon");
        melbourne.addPlayer(banks);

        //teams
        Team<FootballPlayer> hawthorn= new Team<>("Hawthorn");
        Team<FootballPlayer> fremantle= new Team<>("Fremantle");


        System.out.println();
        hawthorn.matchResult(fremantle, 1, 0);
        hawthorn.matchResult(DublinMoorans, 3, 8);

        DublinMoorans.matchResult(fremantle, 2, 1);
        DublinMoorans.matchResult(hawthorn, 1, 1);

        System.out.println("Ranking");
        System.out.println(DublinMoorans.getName() + " : " + DublinMoorans.ranking());
        System.out.println(baseballTeam.getName() + " : " + baseballTeam.ranking());
        System.out.println(brokenTeam.getName() + " : " + brokenTeam.ranking());
        System.out.println(melbourne.getName() + " : " + melbourne.ranking());
        System.out.println(hawthorn.getName() + " : " + hawthorn.ranking());
        System.out.println(fremantle.getName() + " : " + fremantle.ranking());


        System.out.println(DublinMoorans.compareTo(fremantle));
        System.out.println(fremantle.compareTo(DublinMoorans));


    }
}
