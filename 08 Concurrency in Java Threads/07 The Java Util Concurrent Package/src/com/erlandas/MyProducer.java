package com.erlandas;

import java.util.List;
import java.util.Random;

public class MyProducer implements Runnable {

    private List<String> buffer;
    private String color;

    public MyProducer(List<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

    public void run() {
        Random random = new Random();
        String[] nums = {"1","2","3","4","5"};

        for (String num: nums) {
            try {
                System.out.println(color + "Adding...." + num);
                //We synchronizing buffer because that's were problem is
                synchronized (buffer) {
                    buffer.add(num);
                }


                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {
                System.out.println("Producer was interrupted");
            }
        }

        System.out.println(color + "Adding EOF and exiting...");
        //Also synchronize buffer there
        synchronized (buffer) {
            buffer.add("EOF");
        }

    }
}
