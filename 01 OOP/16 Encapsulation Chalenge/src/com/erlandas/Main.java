package com.erlandas;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Printer printer = new Printer(78.5,true);

        printer.fillTonerLevel(5);

        printer.printPages(true,34);
        printer.printPages(false,94);

        printer.printouts();

    }
}
