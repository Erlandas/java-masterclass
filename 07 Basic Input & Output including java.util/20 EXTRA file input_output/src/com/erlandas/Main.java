package com.erlandas;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //BASIC TEXT FILE I/O - CLASSES IN JAVA.IO

        /*
        1.  If you can do console I/O, you can do text file I/O!

        2.  File() - A Java representation of a file. (System.in/System.out)

        3.  PrintWriter() - Used to write to a file, in the same way you write to system.out

        4.  Scanner() - Used to read from a file, in the same way you use it to read from system.in
         */

        //******DEMO: TEXT FILE I/O******

        //******WRITE TO FILE.TXT******

        File file = new File("test.txt");   //-- We create file type object named "test.txt"

        try {
            //Write your name and age to the file
            PrintWriter output = new PrintWriter(file); //-- We create PrintWriter object and referencing to
            //-- Write to create file before
            output.println("Erlandas Bacauskas");       //-- We write details in to a file using println()
            output.println(31);
            output.close();                             //-- IMPORTANT -- close after finished
                                                        //-- It releases a file to use for other programs in OS
        } catch (FileNotFoundException io) {
            System.out.println("FileNotFoundException : " + io.getMessage());
        }

        //******READ FROM A FILE.TXT*******

        try (Scanner input = new Scanner(file)) {  //-- instead of reading from keyboard "System.in" we reading from file
            String name = input.nextLine();         //-- reading from a file first line NAME
            String age = input.nextLine();          //-- reading from a file next line AGE

            System.out.println(name.substring(0,name.indexOf(" ")) + " is " + age + " years old.");

        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException: " + e.getMessage());
        }

    }
}
