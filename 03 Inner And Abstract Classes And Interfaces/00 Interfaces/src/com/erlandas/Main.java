package com.erlandas;

public class Main {

    public static void main(String[] args) {
	    ITelephone erlandasPhone;
	    erlandasPhone = new DeskPhone(851171518);

	    erlandasPhone.powerOn();
	    erlandasPhone.callPhone(851171518);
	    erlandasPhone.answer();

	    erlandasPhone = new MobilePhone(123456);
		erlandasPhone.powerOn();
	    erlandasPhone.callPhone(123456);
	    erlandasPhone.answer();

    }
}
