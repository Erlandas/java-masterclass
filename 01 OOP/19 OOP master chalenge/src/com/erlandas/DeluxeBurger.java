package com.erlandas;

public class DeluxeBurger extends Hamburger {
    public DeluxeBurger() {
        super("Deluxe","Seeded","Bacon & Sausage",9.99);
        super.addAddition1("Chips",2.95);
        super.addAddition2("Drink",1.99);
    }

    @Override
    public void addAddition1(String name, double price) {
        System.out.println("Cannot add additions to a deluxe burger");
    }

    @Override
    public void addAddition2(String name, double price) {
        System.out.println("Cannot add additions to a deluxe burger");
    }

    @Override
    public void addAddition3(String name, double price) {
        System.out.println("Cannot add additions to a deluxe burger");
    }

    @Override
    public void addAddition4(String name, double price) {
        System.out.println("Cannot add additions to a deluxe burger");
    }
}
