package com.erlandas;
/*
java.util.function package :
    That package contains functional interfaces that are meant to be used with lambda expressions.
    Interfaces aren't meant to do anything specific they represent the structure of lambda expressions
    that are commonly used in java.

Consumer :
    It's called consumer because --> object int nothing out.

Predicate :
    Condition passed through method

Function interface :
    Interface represents a function takes one parameter abd returns a value, the functional method the one
    used with lambdas is apply method.

    By using function we can pass code that accepts and returns value to a method in a form of a lambda expression
    and then run that code without having to create the interface and the class that implements the interface,
    we can change what method does based on the function we passed in.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main {

    public static void main(String[] args) {
        Employee john = new Employee("John Doe", 30);
        Employee erlandas = new Employee("Erlandas Bacauskas", 31);
        Employee jack = new Employee("Jack Hill", 40);
        Employee snow = new Employee("Snow White",22);
        Employee red = new Employee("Red RidingHood", 35);
        Employee charming = new Employee("Prince Charming", 31);

        List<Employee> employees = new ArrayList<>();
        employees.add(john);
        employees.add(erlandas);
        employees.add(jack);
        employees.add(snow);
        employees.add(red);
        employees.add(charming);

        System.out.println("\n=================================");
        //Get last name Function :
        //      We used generics with function
        //      The first type is the argument type in this case it's Employee
        //      Second type its the RETURN type in this case it's String
        Function<Employee, String> getLastName = (Employee employee) -> {
            return employee.getName().substring(employee.getName().indexOf(' ') + 1);
        };

        //Get last name of the second employee in the list
        String lastName = getLastName.apply(employees.get(1));
        System.out.println(lastName);

        //Get first name Function :
        Function<Employee, String> getFirstName = (Employee employee) -> {
            return employee.getName().substring(0,employee.getName().indexOf(' '));
        };

        //Use of the function getFirstName
        String firstName = getFirstName.apply(employees.get(1));
        System.out.println(firstName);

        //Now we apply created method getAName

        System.out.println("\n=================================");
        Random random1 = new Random();
        for (Employee employee : employees) {
            if(random1.nextBoolean()) {
                System.out.println("Name : " + getAName(getFirstName,employee));
            } else {
                System.out.println("Surname : " + getAName(getLastName,employee));
            }
            
        }
        
    }

    //--    Method that accepts a function employee string argument
    private static String getAName (Function<Employee,String> getName, Employee employee) {
        return getName.apply(employee);
    }

    //--    Method to print employees by age
    private static void printEmployeesByAge(List<Employee> employees,
                                            String ageText,
                                            //for loop passes each employee to the predicates test method
                                            //which in turn uses the employee as a parameter for the lambda expression
                                            //that maps to the predicate
                                            Predicate<Employee> ageCondition) {
        System.out.println(ageText);
        System.out.println("====================");

        for (Employee employee : employees) {
            if (ageCondition.test(employee)) {  //--    Test method tests condition with passed employee object
                System.out.println(employee.getName());
            }

        }
    }

}
