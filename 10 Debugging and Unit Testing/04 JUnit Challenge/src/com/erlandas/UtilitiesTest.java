package com.erlandas;

import org.junit.Before;

import static org.junit.Assert.*;

public class UtilitiesTest {

    private Utilities util;
    @Before
    public void setup() {
        util = new Utilities();
    }


    @org.junit.Test
    public void everyNthChar() {
        char[] expected = {'e','l'};
        char[] actual = {'h','e','l','l','o'};
        assertArrayEquals(expected,util.everyNthChar(actual,2));
    }

    @org.junit.Test
    public void everyNthChar_nIsGreaterThanArrayLength() {
        char[] actual = {'h','e','l','l','o'};
        assertArrayEquals(actual,util.everyNthChar(actual,7));
    }

    @org.junit.Test
    public void removePairs() {
        assertEquals("ABCDEF",util.removePairs("AABCDDEFF"));
        assertEquals("ABCABDEF",util.removePairs("ABCCABDEEF"));
    }

    @org.junit.Test
    public void removePairs_lessThenTwoChars() {
        assertEquals("A",util.removePairs("A"));
        assertEquals("",util.removePairs(""));

    }
    @org.junit.Test
    public void removePairs_null() {
        assertNull("Did not get null returned argument passed was null",util.removePairs(null));

    }

    @org.junit.Test
    public void converter() {
        assertEquals(300,util.converter(10,5));
    }

    @org.junit.Test(expected = ArithmeticException.class)
    public void converter_exceptionExpected() {
        util.converter(10,0);
        fail("Arithmetic exception was expected");
    }

    @org.junit.Test
    public void nullIfOddLength_expectedNull() {
        assertNull(util.nullIfOddLength("abc"));
    }

    @org.junit.Test
    public void nullIfOddLength_expectedString() {
        assertEquals("abcd",util.nullIfOddLength("abcd"));
    }

}