package com.erlandas;

import static com.erlandas.ThreadColor.*;

public class Main {

    public static void main(String[] args) {
        /*
        We cant guaranty the order in witch threads will be executed
         */

        System.out.println(ANSI_PURPLE +"Hello from the main thread");


        Thread anotherThread = new AnotherThread();
        anotherThread.setName("===Another Thread===");
        //we cant start same thread multiple times only ONCE
        anotherThread.start();

        new Thread() {
            public void run() {
                System.out.println(ANSI_GREEN + "Hello from the anonymous class thread");
            }
        }.start();

//        Thread myRunnableThread = new Thread(new MyRunnable());
//        myRunnableThread.start();


        //Anonymous class of MyRunnable

        Thread myRunnableThread = new Thread(new MyRunnable() {
            @Override
            public void run() {
                //Its running run() from MyRunnable class
                System.out.println(ANSI_RED + "Hello from anonymous class's implementation of run()");
            }
        });
        myRunnableThread.start();

        System.out.println(ANSI_PURPLE + "Hello again from the main thread");
    }
}
