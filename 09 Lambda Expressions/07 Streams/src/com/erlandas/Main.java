package com.erlandas;
/*
Streams in this context are completely unrelated to Input/Output streams

In this example when we refer to stream we refer to sequence of computations

Streams in this context ==> Set of computational steps that are chained together
 */

import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<String> someBingoNumbers = Arrays.asList(
                "N40","N36",
                "B12","B6",
                "G53","G49","G60","G50","g64",
                "I26","I17","I29",
                "O71");

        //--    List to store G numbers
        List<String> gNumbers = new ArrayList<>();

        //--    Same code as below using Streams
        someBingoNumbers
                .stream()
                // .map((String s) -> s.toUpperCase()) Or (String::toUpperCase)
                // :: -> this notation known as method reference
                // using this notation lambda calls existing method
                // In this case .map accepts function
                .map(String::toUpperCase)
                .filter(s->s.startsWith("G"))
                .sorted()
                .forEach(System.out::println);









        //--    Lets say we want to print bingo numbers that are in column "G"
        //--    Using forEach lambda
//        someBingoNumbers.forEach(number -> {
//            if(number.toUpperCase().startsWith("G")) {
//                //--    Add numbers to the list gNumbers
//                gNumbers.add(number);
////                System.out.println(number);
//            }
//        });
//
//        //--    Sort array using lambdas
//        gNumbers.sort((String s1, String s2) -> s1.compareTo(s2));
//        //--    Print sorted array using lambdas
//        gNumbers.forEach((String s) -> System.out.println(s));












    }
}
