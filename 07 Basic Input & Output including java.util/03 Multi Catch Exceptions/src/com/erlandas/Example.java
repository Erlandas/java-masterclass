package com.erlandas;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Example {

    public static void main(String[] args) {
        try {
            int result = divide();
            System.out.println(result);
            // | -> inclusive OR handel multiple exceptions
        } catch (ArithmeticException | NoSuchElementException e) {
            System.out.println(e.toString());
            System.out.println("Unable to perform division, autopilot shutting down");
        }

    }

    private static int divide() {
        int x, y;
//        try {
        x = getInt();
        y = getInt();
        System.out.println("x is " + x);
        System.out.println("y is " + y);
        return x/y;
            /*
            As soon as exception found the exception that happening is executed and remaining catch blocks
            are ignored
             */
//        } catch (NoSuchElementException e) {
//            throw new ArithmeticException("no suitable input");
//        } catch (ArithmeticException e) {
//            throw new ArithmeticException("attempt to divide by zero");
//        }


    }

    private static int getInt() {
        Scanner s = new Scanner(System.in);
        System.out.println("Please enter integer ");

        while(true) {
            try {
                return s.nextInt();
            } catch (InputMismatchException e) {
                //go round again. Read past the end of line in the input first
                s.nextLine();
                System.out.println("Please enter s number using only digits 0 to 9");
            }
        }






    }
}
