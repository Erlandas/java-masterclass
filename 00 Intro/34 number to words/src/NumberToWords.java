public class NumberToWords {

    //Starts numberToWords
    public static void numberToWords(int number) {

        if(number < 0) {
            System.out.println("Invalid Value");
        }
        else if(number == 0) {
            System.out.println("Zero");
        }
        else {

            int reverse = reverse(number);
            while (reverse > 0) {

                switch (reverse % 10) {
                    case 0:
                        System.out.println("Zero");
                        break;
                    case 1:
                        System.out.println("One");
                        break;
                    case 2:
                        System.out.println("Two");
                        break;
                    case 3:
                        System.out.println("Three");
                        break;
                    case 4:
                        System.out.println("Four");
                        break;
                    case 5:
                        System.out.println("Five");
                        break;
                    case 6:
                        System.out.println("Six");
                        break;
                    case 7:
                        System.out.println("Seven");
                        break;
                    case 8:
                        System.out.println("Eight");
                        break;
                    case 9:
                        System.out.println("Nine");
                        break;
                }
                reverse /= 10;
            }

            for(int i = 0 ; i < (getDigitCount(number) - getDigitCount(reverse(number))); i++) {
                System.out.println("Zero");
            }
        }
    }
    //Ends numberToWords

    //Starts reverse
    public static int reverse(int number) {
        int reversed = 0;
        int negative = 1;

        if (number < 0) {
            negative = -1;
            number *= -1;
        }

        while(number > 0) {
            reversed = reversed + number % 10;

            if (number >= 10) {
                reversed *= 10;
            }

            number /= 10;
        }

        return reversed * negative;
    }
    //Ends reverse

    //Starts getDigitCount
    public static int getDigitCount(int number) {

        if(number < 0) return -1;
        if(number == 0) return 1;

        int count = 0;

        while(number > 0) {
            count++;
            number /= 10;
        }

        return count;
    }
    //Ends getDigitCount

}
