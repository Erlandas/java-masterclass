package com.erlandas;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class League<T extends Team>  {

    public String name;
    public ArrayList<T> league = new ArrayList<>();

    public League(String name) {
        this.name = name;
    }

    public boolean add(T team) {
        if(league.contains(team)) {
            System.out.println("Team is already there no duplicates allowed");
            return false;
        }

        league.add(team);
        System.out.println("'" + team.getName() + "' team is added");
        return true;
    }

    public void showLeagueTable() {
        Collections.sort(league);
        for (T i: league) {
            System.out.println(i.getName() + ": " + i.ranking());
        }
    }
}
