package com.erlandas;

public class Dimensions {
    public int width;
    public int height;

    public Dimensions(int width, int height) {
        this.width = width;
        this.height = height;
    }


    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
