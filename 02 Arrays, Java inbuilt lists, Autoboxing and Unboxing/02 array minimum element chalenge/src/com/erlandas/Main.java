package com.erlandas;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int count;

        System.out.println("How many integer values would you like to enter? :\r");
        count = scanner.nextInt();

        int[] arrayOfIntegerValues = readIntegers(count);

        System.out.println("Minimum value from entered integer values is --> " + findMin(arrayOfIntegerValues));


    }


    //READ INTEGERS
    public static int[] readIntegers(int count) {
        int[] arrayOfintegers = new int[count];

        System.out.println("Enter " +count+ " integer values");
        for (int i = 0 ; i < count ; i++) {
            arrayOfintegers[i] = scanner.nextInt();
        }

        return arrayOfintegers;
    }

    //FIND MIN VALUE
    public static int findMin(int[] array) {
        int min = array[0];

        for (int i = 1 ; i < array.length ; i++) {
            if(min > array[i]) {
                min = array[i];
            }
        }

        return min;
    }
}
