package com.erlandas;

//BASE CLASS
class Car {
    private int cylinders;
    private boolean engine;
    private int wheels;
    private String name;

    //constructor
    public Car(int cylinders, String name) {
        this.cylinders = cylinders;
        this.name = name;
        this.engine = true;
        this.wheels = 4;
    }

    //getters
    public int getCylinders() {
        return cylinders;
    }

    public String getName() {
        return name;
    }

    //methods
    public void startEngine() {
        System.out.println("Car -> startEngine");
        System.out.println(this.name + " Starts Engine");
    }

    public void accelerate() {
        System.out.println("Car -> accelerate");
        System.out.println(this.name + " accelerates");
    }

    public void brake() {
        System.out.println("Car -> brake");
        System.out.println(this.name + " brakes");
    }
}

class Audi extends Car {
    public Audi() {
        super(8, "Audi Quatro");
    }

    @Override
    public void startEngine() {
        super.startEngine();
        System.out.println("Its starting with POWER of Quatro");
    }

    @Override
    public void accelerate() {
        super.accelerate();
        System.out.println("Its accelerates with speed of light");
    }

    @Override
    public void brake() {
        super.brake();
        System.out.println("It brakes like there is no tomorrow");
    }
}

class Nissan extends Car {
    public Nissan() {
        super(4, "Nissan Micra");
    }

    @Override
    public void startEngine() {
        super.startEngine();
        System.out.println("Is it started or not?!");
    }

    @Override
    public void accelerate() {
        super.accelerate();
        System.out.println("It accelerates like a turtle");
    }

    @Override
    public void brake() {
        super.brake();
        System.out.println("It takes little time to brake as it was bearly mooving");
    }
}

class Honda extends Car {
    public Honda() {
        super(6, "Honda Civic");
    }

    @Override
    public void startEngine() {
        super.startEngine();
        System.out.println("It starts as quick as it gets");
    }

    @Override
    public void accelerate() {
        super.accelerate();
        System.out.println("Accelerates as there is no tommorrow");
    }

    @Override
    public void brake() {
        super.brake();
        System.out.println("It brakes as it wasnt mooving");
    }
}


//MAIN CLASS
public class Main {

    public static void main(String[] args) {
	// write your code here
        for (int i = 0 ; i <= 10 ; i++) {
            Car car = randomCar();
            System.out.println("Car #"+i+" : " +car.getName()+
                    "has " +car.getCylinders()+ " cylinders");

            car.startEngine();
            car.accelerate();
            car.brake();
            System.out.println("\n");
        }
    }

    public static Car randomCar() {
        int randomNumber = (int) (Math.random()*3)+1;
        System.out.println("Random number generated " + randomNumber);
        switch(randomNumber) {
            case 1:
                return new Audi();

            case 2:
                return new Nissan();

            case 3:
                return new Honda();

            default:
                return null;
        }
    }
}
