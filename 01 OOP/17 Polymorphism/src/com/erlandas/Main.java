package com.erlandas;


//BASE CLASS
class Movie {
    private String name;

    public Movie(String name) {
        this.name = name;
    }

    public String plot() {
        return "No plot here";
    }

    public String getName() {
        return name;
    }
}

//SUBCLASSES USING INHERITANCE
class Jaws extends Movie {
    public Jaws() {
        super("Jaws");
    }

    public String plot() {
        return "Shark eats lots of people";
    }
}

class IndependenceDay extends Movie {
    public IndependenceDay() {
        super("Independence Day");
    }

    @Override
    public String plot() {
        return "Aliens try to take over planet earth";
    }
}

class MazeRunner extends Movie {
    public MazeRunner() {
        super("Maze Runner");
    }

    @Override
    public String plot() {
        return "Kids try and escape a maze";
    }
}

class StarWars extends Movie {
    public StarWars() {
        super("Star Wars");
    }

    @Override
    public String plot() {
        return "Imperial forces try to take over the universe";
    }
}

class Forgetable extends Movie {
    public Forgetable() {
        super("Forgetable");
    }

    //NO PLOT METHOD
}



//MAIN CLASS
public class Main {

    public static void main(String[] args) {
	// Now we apply polymorphism to call plot method for randomly chosen movie
        for (int i = 0 ; i < 11 ; i++) {
            Movie movie = randomMovie();
            System.out.println("Movie #" +i+ " : " +movie.getName() + "\n" +
                    "Plot : " + movie.plot() + "\n");
        }
        //movie.plot() thats the basic polymorphism it decides from what class to execute plot() method by chosen movie


    }

    //Thats inheritance
    public static Movie randomMovie() {
        int randomNumber = (int) (Math.random()*5) + 1;
        System.out.println("Random number generated " + randomNumber);

        switch(randomNumber) {
            case 1:
                return new Jaws();

            case 2:
                return new IndependenceDay();

            case 3:
                return new MazeRunner();

            case 4:
                return new StarWars();

            case 5:
                return new Forgetable();
        }

        return null;

    }
}
