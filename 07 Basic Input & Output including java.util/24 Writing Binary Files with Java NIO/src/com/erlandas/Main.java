package com.erlandas;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        try (FileOutputStream binFile = new FileOutputStream("data.dat");
             FileChannel binChannel = binFile.getChannel()) {
            /*
            Buffers capacity -> its the number of elements buffer can contain

            Buffers position -> is the index of the next element that should be read or
                                written it cant be greater the buffers capacity

            Buffers mark ->     used by the buffer.reset method when this method called
                                buffers position reset to its mark

                   When you want to rewind buffer to certain point you mark the point
                   then later call the reset method to reset the position to the mark
             */


            //                             --getBytes() called to create byte array
            byte[] outputBytes = "Hello World!".getBytes();

            //-- to create byte buffer from the byte array
            //
            //-- wrap method does the following
            //-1-    It wraps the byte array to the buffer
            //-2-    sets the position of the buffer to 0
            //-3-    buffers capacity will be set to byte arrays length
            //-4-    buffers mark will be undefined
            ByteBuffer buffer = ByteBuffer.wrap(outputBytes);

            int numBytes = binChannel.write(buffer); //this method returns Int

            System.out.println("numBytes written was: " + numBytes);

            //-- we calling ByteBuffer.allocate() method we passing the size we want buffer to be
            //   in this case its the number of bytes in Integer
            ByteBuffer intBuffer = ByteBuffer.allocate(Integer.BYTES);

            intBuffer.putInt(245);
            intBuffer.flip(); //-- to reset the position to 0
            numBytes = binChannel.write(intBuffer);
            System.out.println("numBytes written was: " + numBytes);

            intBuffer.flip(); //-- to reset the position to 0
            intBuffer.putInt(-98765);
            intBuffer.flip(); //-- to reset the position to 0
            numBytes = binChannel.write(intBuffer);
            System.out.println("numBytes written was: " + numBytes);


            /*
            We have to call buffer.flip() method always when we transferring from writing to reading
            and vise versa
             */


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
