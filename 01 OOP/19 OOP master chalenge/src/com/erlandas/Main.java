package com.erlandas;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Hamburger burger = new Hamburger("Double decker","White","Beef",4.95);
        burger.addAddition1("cheese",0.60);
        burger.addAddition2("tomato",0.25);
        burger.addAddition3("gherkins",0.20);


        //System.out.println("________________________________\nTotal price : " + burger.displayOrderTotalPrice() + "$");

        HealthyBurger burgerH = new HealthyBurger("turkey",5.0);
        burgerH.addHealthyExtra1("Egg",2.0);
        burgerH.addHealthyExtra2("lentils",3.0);
        burgerH.addAddition1("Heart",9.99);

        System.out.println("________________________________\nTotal price: " + burgerH.displayOrderTotalPrice());

        DeluxeBurger delux = new DeluxeBurger();
        //delux.addAddition1("Corn",123);

        //System.out.println("____________________________\nTotal price : " +delux.displayOrderTotalPrice());


    }
}
