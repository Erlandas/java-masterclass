/*
Immutable classes is a great way to increase encapsulation
protects an object from external modification
 */
package com.erlandas;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Location {

    private final int locationID;
    private final String description;
    private final Map<String, Integer> exists;

    public Location(int locationID, String description, Map<String, Integer> exits) {
        this.locationID = locationID;
        this.description = description;

        if(exits != null) {
            this.exists = new LinkedHashMap<String, Integer>(exits);
        } else {
            this.exists = new LinkedHashMap<String, Integer>();
        }

        this.exists.put("Q",0);
    }

    //Getters
    public int getLocationID() {
        return locationID;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, Integer> getExists() {
        //instead of just returning exists map im creating a new HashMap and passing out exists in the constructor
        //Getter return copy of exists
        //For safety reasons that exists wont be changed as returned
        return new LinkedHashMap<String, Integer>(exists);
    }

    protected void addExit(String direction, int location) {
        exists.put(direction,location);
    }
}
