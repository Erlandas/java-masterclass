public class Main {

    private static final String INVALID_VALUE_MESSAGE = "Invalid Value";

    public static void main(String[] args) {

        System.out.println(getDurationString(65,5));

    }
/*
    public static String getDurationString(int minutes, int seconds) {

        if(minutes >= 0 && (seconds >= 0 && seconds <= 59)) {
            return minutes/60 + "h " + minutes%60 + "m " + seconds + "s";
        }

        return "Invalid Value";
    }
*/

    //BONUS to add leading 0 beside hours minutes and seconds if needed

    public static String getDurationString(int minutes, int seconds) {

        if(minutes >= 0 && (seconds >= 0 && seconds <= 59)) {

            return addZero(minutes/60) + "h " + addZero(minutes%60) + "m " + addZero(seconds) + "s";
        }

        return INVALID_VALUE_MESSAGE;
    }

    public static String addZero(int timeNumber) {

        if(timeNumber < 10) {
            return "0" + timeNumber;
        }

        return "" + timeNumber;

    }

    public static String getDurationString(int seconds) {

        if(seconds >= 0) {
            return getDurationString((seconds/60), (seconds%60));
        }

        return INVALID_VALUE_MESSAGE;
    }
}
