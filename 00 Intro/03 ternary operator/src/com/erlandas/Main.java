package com.erlandas;

public class Main {

    public static void main(String[] args) {

        //Ternary: this is a way to set a value based on conditions

        boolean isCar = false;

        //if is car is evaluated to true so we return true otherwise we return false
        //? mark asks if car is evaluated to true
        //result of a wasCar depends on reult of isCar
        boolean wasCar = isCar ? true : false;

        if (wasCar)
        {
            System.out.println("wasCar was true");
        }
        else
        {
            System.out.println("wasCar was false");
        }
    }
}
