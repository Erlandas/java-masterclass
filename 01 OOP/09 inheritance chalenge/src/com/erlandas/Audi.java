package com.erlandas;


public class Audi extends Car {

    public Audi(double weight, double wheelSize, int wheelCount, double topSpeed, int doorCount,
                int sittingPlaces, double engineSize, String model) {
        super(weight, wheelSize, wheelCount, topSpeed, doorCount, sittingPlaces, engineSize, model);
    }

    @Override
    public void carStearing(String position) {
        super.carStearing(position);
    }

    @Override
    public void starts() {
        System.out.print("Audi ");
        super.starts();
    }
}
