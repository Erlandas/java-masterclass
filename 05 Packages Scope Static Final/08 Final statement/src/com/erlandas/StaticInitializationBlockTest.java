package com.erlandas;
/*
Static Initialization Blocks
are called in order that declared in a class
 */
public class StaticInitializationBlockTest {
    public static final String owner;

    static {
        owner = "Erlandas";
        System.out.println("Static Initialization block called");
    }

    public StaticInitializationBlockTest() {
        System.out.println("SIB constructor called");
    }

    static {
        System.out.println("Second initialization block called");
    }

    public void someMethod() {
        System.out.println("someMethod called");
    }
}
