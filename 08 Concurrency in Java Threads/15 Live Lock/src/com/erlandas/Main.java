package com.erlandas;
    /*
    LIVE LOCK:
        --  Live lock is similar to deadlock but instead of threads being locked they actually
            constantly active and usually waiting for all the other threads to complete their
            tasks. Since all the threads are waiting for all the others to complete, none of
            them can actually progress.

        --  Lets say Thread A will loop until Thread B completes his tasks
            And Thread B will loop until Thread A completes its tasks

        --  Thread A and B gets in to state when they looping and waiting for other Threads to complete.

        --  In this case Threads ar never progress but they ar not actually blocked


     */

public class Main {

    public static void main(String[] args) {

        final Worker worker1 = new Worker("Worker 1", true);
        final Worker worker2 = new Worker("Worker 2", true);

        final SharedResource sharedResource = new SharedResource(worker1);

        new Thread(new Runnable() {
            @Override
            public void run() {
                worker1.work(sharedResource, worker2);
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                worker2.work(sharedResource,worker1);
            }
        }).start();




















    }

}
