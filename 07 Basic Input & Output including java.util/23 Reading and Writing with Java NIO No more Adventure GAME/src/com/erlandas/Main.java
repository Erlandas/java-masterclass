package com.erlandas;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        //************READ DATA******************
        try {

//            FileInputStream file = new FileInputStream("data.txt");
//            FileChannel channel = file.getChannel();

            Path dataPath = FileSystems.getDefault().getPath("data.txt");


            /*
            Code to write some data to a file
            -- this method writes bytes not strings getBytes() converts string to a bytes
            */

            Files.write(dataPath,"\nline 7".getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);

            List<String> lines = Files.readAllLines(dataPath);
            for (String line : lines) {
                System.out.println(line);
            }




        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
