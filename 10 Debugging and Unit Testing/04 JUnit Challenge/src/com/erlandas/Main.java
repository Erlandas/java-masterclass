package com.erlandas;

/**
 * Challenge #1 : Set up test stubs
 *      Create a JUnit test class that contains a test method for each method in the
 *      Utilities class. Don't add any test code to the methods yet. When you run the
 *      tests, they should all fail. use the naming conventions we discussed in the JUnit
 *      lecture. Remember that you'll have to add the JUnit4 library to the test project.
 */

/**
 * Challenge #2 :   Add test code to the removePairs() test method, to test the
 *                  Utilities.removePairs() method.
 *
 *      In this challenge, you'll create test for the utilities.removePairs() method. This
 *      method accepts a string and removes any pairs it contains by removing one half of
 *      the pair. For example:
 *      Input/output:
 *      "AABCDDEFF" -> "ABCDEF"
 *      "ABCCABDEEF" -> "ABCABDEF"
 *
 *      Write the test code that tests the above two scenarios. start by writing a test for
 *      the first set of input/output. Once that test passes, add a test for the second
 *      input/output pair. To keep things simple, you can add the second test to the
 *      same test method, but if you want to be strict about it, you can create another
 *      test method for it
 */

/**
 * Challenge #3 :
 *      come up with two more tests for the removePairs() method
 */

/**
 * Challenge #4 :   Add a test for the everyNthChar() method
 *
 *      Now write a test for the everyNthChar() method that tests the following scenario :
 *      Input: char array containing the letters 'h','e','l','l','o', in that order, and n = 2
 *      Output: char array containing the letters 'e','l', in that order
 */

/**
 * challenge #5 :
 *      Add a test for the method everyNthChar when n is greater than the length of the array
 */

/**
 * Challenge #6 :
 *      add test for the nullIfOddlength() method
 *      write two test for this method one expecting null other expecting string back
 */

/**
 * Challenge #7 :
 *      add test for the converter
 *
 *      Test the coverter() method with the following input/output
 *      input: a = 10, b = 5
 *      output: 300
 */

/**
 * Challenge #8 :
 *      Write test for converted to catch arithmetic exception division by 0
 */

/**
 * Challenge #9 :
 *      Remove repetition of code in test class (instantiating utilities class)
 */
public class Main {


    public static void main(String[] args) {

        Utilities util = new Utilities();
        System.out.println(util.removePairs("AABCDDEFFFFF"));


    }
}
