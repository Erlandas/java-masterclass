import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        boolean isAnInt;

        while(true) {

            System.out.println("Enter number: ");
            isAnInt = scanner.hasNextInt();

            if(isAnInt) {

                int currentNum = scanner.nextInt();

                if(min > currentNum) {
                    min = currentNum;
                }

                if(max < currentNum) {
                    max = currentNum;
                }

            } else {
                System.out.println("Invalid Value Bye Bye");
                break;
            }

            System.out.println("Current Min = " + min);
            System.out.println("Current Max = " + max);

            scanner.nextLine();

        }

        scanner.close();
    }
}
