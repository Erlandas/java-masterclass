package com.example.mypackage;

import java.util.ArrayList;

public class Series {

    // nSum(int n) returns the sum of all numbers from 0 to n. The first 10 numbers are:
    // 0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55.
    public int nSum(int n) {
        return (n * (n + 1)) / 2;
    }

    // factorial(int n) returns the product of all numbers from 1 to n
    //      i.e. 1 * 2 * 3 * 4 ... * (n - 1) * n.
    // The first 10 factorials are:
    // 0, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800.

    public int factorial(int n) {
        if(n == 0) {
            return 1;
        }
        int factorial = 1;


        for (int i = 1; i <= n ; i++) {
            factorial*=i;
        }
        return factorial;
    }

    // fibonacci(n) returns the nth Fibonacci number. These are defined as:
    // f(0) = 0
    // f(1) = 1
    // f(n) = f(n-1) + f(n-2)
    // (so f(2) is also 1. The first 10 fibonacci numbers are:
    // 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55.

    public int fibonacci(int f) {
        ArrayList<Integer> fNum = new ArrayList<>();
        fNum.add(0,0);
        fNum.add(1,1);

        if(f == 0 || f == 1) {
            return fNum.get(f);
        }

        for(int i = 2 ; i <= f ; i++) {
            int nextFib = fNum.get(fNum.size()-1)+fNum.get(fNum.size()-2);
            fNum.add(i,nextFib);
        }

        return fNum.get(fNum.size()-1);
    }

    public long timsFibonacci(int f) {
        if(f == 0) return 0;
        else if(f == 1) return 1;

        long fibOne = 1;
        long fibTwo = 0;
        long fib = 0;

        for (int i = 1 ; i < f ; i++) {
            fib = fibOne + fibTwo;
            fibTwo = fibOne;
            fibOne = fib;
        }

        return fib;


    }
}
