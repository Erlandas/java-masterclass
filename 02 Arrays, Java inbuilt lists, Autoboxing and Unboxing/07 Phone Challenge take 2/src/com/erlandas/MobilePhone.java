package com.erlandas;

import java.util.ArrayList;

public class MobilePhone {

    private ArrayList<Contact> myContacts = new ArrayList<Contact>();

    public MobilePhone() {
        this.myContacts = new ArrayList<Contact>();
    }


    public int findContact(Contact contact) {
        return this.myContacts.indexOf(contact);
    }

    public int findContact(String contactName) {
        for (int i = 0 ; i < this.myContacts.size() ; i++) {
            Contact contact = this.myContacts.get(i);
            if(contact.getName().equals(contactName)) {
                return i;
            }
        }
        return -1;
    }

    public boolean updateContact(Contact oldContact,Contact newContact) {
        int position = findContact(oldContact);
        if(position < 0 ) {
            System.out.println(oldContact.getName() + ", wasnt found" + findContact(oldContact));
            return false;
        } else if(findContact(newContact.getName()) != -1) {
            System.out.println("New contact exists ERROR");
            return false;
        }

        this.myContacts.set(position,newContact);
        System.out.println(oldContact.getName() + ", was replaced with " + newContact.getName());
        return true;
    }

    public boolean removeContact(Contact contact) {
        if(findContact(contact) < 0) {
            System.out.println(contact.getName() + ", Was not found");
            return false;
        }

        System.out.println(contact.getName() + ", removed");
        this.myContacts.remove(findContact(contact));
        return true;

    }

    public String queryContact(Contact contact) {
        if(findContact(contact) < 0) {
            return null;
        }

        return contact.getName();
    }

    public Contact queryContact(String name) {
        int position = findContact(name);
        if(position >= 0) {
            return  this.myContacts.get(position);
        }
        return null;
    }

    public void printContacts() {
        for (int i = 0 ; i < this.myContacts.size() ; i++) {
            Contact contact = this.myContacts.get(i);
            System.out.println((i+1)+ ". "+contact.getName()+" "+contact.getNumber());
        }
    }

    public boolean addContact(Contact contact) {
        if(findContact(contact.getName()) >= 0 ) {
            System.out.println(contact.getName() + ", Contact already exists" + findContact(contact.getName()));
            return false;
        }
        this.myContacts.add(contact);
        System.out.println(contact.getName() + ", Contact added" + findContact(contact.getName()));
        return true;
    }
}
