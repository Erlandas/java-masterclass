package com.erlandas;

public interface ITelephone {

    /*
    We set for class thats its going to implements from this interface
    these are the methods that has to implememnt
    so no code written in the interface

    We just define contract eg. methods parameters return types in the interface
     */
    void powerOn();
    void dial(int phoneNumber);
    void answer();
    boolean callPhone(int phoneNumber);
    boolean isRinging();

}
