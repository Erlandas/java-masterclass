package com.erlandas;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        //Not generic example first
//        ArrayList items = new ArrayList();
//        items.add(1);
//        items.add(2);
//        items.add(3);
//        items.add(4);
//        items.add(5);

        //Generics example
        ArrayList<Integer> items = new ArrayList<>();
        items.add(1);
        items.add(2);
        items.add(3);
        items.add(4);
        items.add(5);


        printDoubled(items);
    }

    //NON generic method example
//    public static void printDoubled(ArrayList n) {
//        for (Object i: n) {
//            System.out.println((Integer) i*2);
//        }
//    }

    //Generic method example
    private static void printDoubled(ArrayList<Integer> n) {
        for (int i: n) {
            System.out.println(i*2);
        }
    }
}
