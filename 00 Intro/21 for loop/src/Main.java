public class Main {

    public static void main(String[] args) {

        //ONE
        for (int i = 2 ; i <= 8 ; i++) {
            //System.out.println("10,000 at " + (double)(i) + "% interest = " + String.format("%.2f",calculateInterest(10000,(i))));
        }

        //TWO
        for (int i = 8 ; i >= 2 ; i--) {
            //System.out.println("10,000 at " + (double)(i) + "% interest = " + String.format("%.2f",calculateInterest(10000,(i))));
        }

        //THREE
        int primeCount = 0;

        for (int i = 131; i < 999 ; i++) {
            if(isPrime(i)) {
                System.out.println(i);
                primeCount++;

                if(primeCount == 3) {
                    break;
                }
            }
        }


    }

    private static double calculateInterest(double amount, double interestRate) {
        return amount * (interestRate/100);
    }

    private static boolean isPrime(int n) {
        if(n == 1) {
            return false;
        }

        /*
        n/2 used for optimization otherwise we will be using only N
        to optimize even more we can use Math.sqrt(n)
         */

        for(int i = 2 ; i <= Math.sqrt(n) ; i++) {
            System.out.println("Looping " + i);
            if(n%i==0) {
                return false;
            }
        }

        return true;
    }
}
