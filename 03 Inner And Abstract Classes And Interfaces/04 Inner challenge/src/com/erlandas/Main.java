package com.erlandas;

import java.util.*;

public class Main {

    private static ArrayList<Album> albums = new ArrayList<Album>();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {


        play(addTestSongs());


    }

    private static void play(LinkedList<Song> playList) {

        boolean quit = false;
        boolean goingForward = true;

        ListIterator<Song> songsIterator = playList.listIterator();

        if(playList.size() == 0) {
            System.out.println("No songs added to playlist");
            return;
        } else {
            System.out.println("Now playing " + songsIterator.next().toString());
            printActions();
        }

        String currentSong = "";
        while(!quit) {
            int action = scanner.nextInt();
            scanner.nextLine();

            switch(action) {
                case 0:
                    System.out.println("Shutting down...");
                    quit = true;
                    break;

                case 1:

                    //PLAYING NEXT SONG
                    if(!goingForward){
                        if(songsIterator.hasNext()) {
                            songsIterator.next();
                        }
                        goingForward = true;
                    }

                    if(songsIterator.hasNext()) {
                        currentSong = songsIterator.next().toString();
                        System.out.println("Now playing " + currentSong);

                    } else {
                        System.out.println("You reached end of playlist");
                        currentSong = "You reached end of playlist";
                        goingForward = false;
                    }

                    break;

                case 2:

                    //PLAY previous song
                    if(goingForward){
                        if(songsIterator.hasPrevious()) {
                            songsIterator.previous();
                        }
                        goingForward = false;
                    }

                    if(songsIterator.hasPrevious()) {
                        currentSong = songsIterator.previous().toString();
                        System.out.println("Now playing " + currentSong);
                    } else {
                        System.out.println("You reached start of playlist");
                        currentSong = "You reached start of playlist";
                        goingForward = true;
                    }


                    break;

                case 3:
                    /*
                    if(songsIterator.hasPrevious()){
                        System.out.println("Now playing " + songsIterator.previous().toString());
                        songsIterator.next();
                    } else {
                        System.out.println("Now playing " + songsIterator.next().toString());
                    }
                    */

                    //System.out.println("Now playing " + currentSong);

                    if(goingForward) {
                        if(songsIterator.hasPrevious()) {
                            System.out.println("Now playing" + songsIterator.previous());
                            goingForward = false;
                        } else {
                            System.out.println("We atr the strat");
                        }
                    } else {
                        if (songsIterator.hasNext()) {
                            System.out.println("Now playing" + songsIterator.next());
                            goingForward = true;
                        } else {
                            System.out.println("We at the end");
                        }
                    }
                    break;

                case 4:

                    /*
                    for(Song song: playList) {
                        System.out.println("\t"+ song.toString());

                    }*/

                    printList(playList);

                    break;

                case 5:
                    printActions();
                    break;

                case 6:
                    //remove song from playlist
                    if(playList.size() > 0) {
                        songsIterator.remove();
                        if(songsIterator.hasNext()) {
                            System.out.println("Now playing " + songsIterator.next());
                        } else if (songsIterator.hasPrevious()) {
                            System.out.println("Now playing " + songsIterator.previous());
                        }
                    }
                    break;
                default:
                    System.out.println("Choose from menu options (5 - to print menu options)");
                    break;
            }
        }
    }

    //Example how to use ordinary iterator
    //its useful when we going true items no function to go backwards
    private static void printList(LinkedList<Song> songs) {
        Iterator<Song> songIterator = songs.iterator();
        System.out.println("=============================");
        while(songIterator.hasNext()) {
            System.out.println(songIterator.next());
        }
        System.out.println("=============================");
    }

    private static void printActions() {
        System.out.println("Player: Playlist\nPress:");
        System.out.println("0 - to EXIT\n" +
                "1 - to play next song\n" +
                "2 - to play previous song\n" +
                "3 - to replay current song\n" +
                "4 - to print playlist\n" +
                "5 - to print menu options\n" +
                "6 - to delete current song from playlist");
    }

    private static LinkedList addTestSongs() {
        Album album = new Album("Stormbringer", "Deep Purple");
        album.addSong("Stormbringer", 4.6);
        album.addSong("Love don't mean a thing", 4.22);
        album.addSong("Holy man", 4.3);
        album.addSong("Hold on", 5.6);
        album.addSong("Lady double dealer", 3.21);
        album.addSong("You can't do it right", 6.23);
        album.addSong("High ball shooter", 4.27);
        album.addSong("The gypsy", 4.2);
        album.addSong("Soldier of fortune", 3.13);
        albums.add(album);

        album = new Album("For those about to rock", "AC/DC");
        album.addSong("For those about to rock", 5.44);
        album.addSong("I put the finger on you", 3.25);
        album.addSong("Lets go", 3.45);
        album.addSong("Inject the venom", 3.33);
        album.addSong("Snowballed", 4.51);
        album.addSong("Evil walks", 3.45);
        album.addSong("C.O.D.", 5.25);
        album.addSong("Breaking the rules", 5.32);
        album.addSong("Night of the long knives", 5.12);
        albums.add(album);

        LinkedList<Song> playList = new LinkedList<Song>();
        albums.get(0).addToPlayList("You can't do it right", playList);
        albums.get(0).addToPlayList("Holy man", playList);
        albums.get(0).addToPlayList("Speed king", playList);  // Does not exist
        albums.get(0).addToPlayList(9, playList);
        albums.get(1).addToPlayList(8, playList);
        albums.get(1).addToPlayList(3, playList);
        albums.get(1).addToPlayList(2, playList);
        albums.get(1).addToPlayList(24, playList);  // There is no track 24

        return playList;
    }
}
