package com.erlandas;
//Now we create class that is made up 3 other classes case, motherboard and monitor
public class PC {
    private Case theCase;
    private Monitor monitor;
    private Motherboard motherboard;

    public PC(Case theCase, Monitor monitor, Motherboard motherboard) {
        this.theCase = theCase;
        this.monitor = monitor;
        this.motherboard = motherboard;
    }

    public void powerUp() {
        this.theCase.pressPowerButton();
        drawLogo();
    }

    private void drawLogo() {
        //Fance graphics
        this.monitor.drawPixelAt(1200,50,"Yellow");
    }

    /* We could change getters to private methods and acces them through PC class directly
    public Case getTheCase() {
        return theCase;
    }

    public Monitor getMonitor() {
        return monitor;
    }

    public Motherboard getMotherboard() {
        return motherboard;
    }
     */
}
