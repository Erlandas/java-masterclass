package com.erlandas;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String albumName;
    private String albumArtist;
    private SongList songs;

    public Album(String albumName, String albumArtist) {
        this.albumName = albumName;
        this.albumArtist = albumArtist;
        this.songs = new SongList();
    }


    //add song to the album
    public boolean addSong(String title, double duration) {
        return this.songs.add(new Song(title,duration));
    }


    //Add song by tract number PLAYLIST
    public boolean addToPlayList(int trackIndex, LinkedList<Song> playlist) {

        Song checkedSong = this.songs.findSong(trackIndex);
        if(checkedSong != null) {
            playlist.add(checkedSong);
            return true;
        }
        return false;
    }

    //Add song by title PLAYLIST
    public boolean addToPlayList(String songTitle, LinkedList<Song> playlist) {
        Song checkedSong = songs.findSong(songTitle);
        if(checkedSong != null) {
            playlist. add(checkedSong);
            System.out.println("Added");
            return true;
        }

        System.out.println("NOT Added");
        return false;
    }

    ///////////////////////////////////////////////////////////////////////

    class SongList {
        private ArrayList<Song> songs;

        public SongList() {
            this.songs = new ArrayList<Song>();
        }

        public boolean add(Song song) {
            if(songs.contains(song)) {
                return false;
            }
            songs.add(song);
            return true;
        }

        public Song findSong(String title) {
            for (int i = 0 ; i < songs.size() ; i++) {
                Song currentSong = this.songs.get(i);
                if(currentSong.getTitle().equals(title)) {
                    return currentSong;
                }
            }
            return null;
        }

        public Song findSong(int trackNumber) {
            int index = trackNumber-1;
            if(trackNumber >= 0 && trackNumber < songs.size()) {
                return songs.get(index);
            }
            return null;
        }

    }

























}
