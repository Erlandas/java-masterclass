package com.company;

public class Main {

    public static void main(String[] args) {
	    //<<<<<<<<<<<<FLOAT & DOUBLE>>>>>>>>>>>>>>
        //Float is single precision and a double is double precision numbers

        //int is 32 (4 bytes)
        int myIntValue = 5;

        //if a number with a decimal point value 5.25 java will asume if its double or float dont need f or d
        //float is 32 (4 bytes)
        float myFloatValue = 5f;        //correct way leave f
        //double is 64 (4 bytes)
        double myDoubleValue = 5d;      //correct way leave d

        System.out.println("Double is more precise number");
        System.out.println("float 5 / 3 = " + (5f/3f));
        System.out.println("Double 5 / 3 = " + (5d/3d));

        //convert pounds to kilograms and store in to a variable
        //1 pound = 0.45359237

        double amountOfPounds = 200d;
        double onePoundWorth = 0.453592d;
        double convert = amountOfPounds * onePoundWorth;

        System.out.println(amountOfPounds + " LB converted to KG = " + convert);

        // to put unicode characters in char
        //char width of 16 (2 bytes)
        char unicode = '\u00a9';
        System.out.println("\n\nhttps://unicode-table.com/en/");
        System.out.println("u00a9 = " + unicode);

        char registered = '\u00ae';
        System.out.println("u00ae = " + registered);

        //PRIMITIVE DATA TYPES IN JAVA:
            //int
            //long
            //short
            //byte
            //char
            //boolean
            //float
            //double

        //we can use unicode char in a string also example below :
        String exampleUnicode = "my string is \u00ae 2015";
        System.out.println("\n" + exampleUnicode);

    }
}
