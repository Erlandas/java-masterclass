package com.erlandas;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Integer> integerArrayList = new ArrayList<Integer>();

        for (int i = 0 ; i <= 10 ; i++) {
            //todo Integer.valueOf(i) this part here is autoboxing we converting an integer base primitive type int to an integer
            integerArrayList.add(Integer.valueOf(i));
        }

        for (int i = 0 ; i < integerArrayList.size() ; i++) {
            //todo integerArrayList.get(i).intValue() thats unboxing we taking non-priminitive type and going to object class
            //and we converting it back to a primitive type
            System.out.println(integerArrayList.get(i).intValue());
        }

        //we can type like this but at compile time will be translated to this Integer.valueOf(56)
        Integer myIntValue = 56; //Integer.valueOf(56);
        int myInt = myIntValue; //myIntValue.intValue();

        System.out.println("\n\n");
        ArrayList<Double> myDoubleValues = new ArrayList<Double>();

        for (double dbl = 0.0 ; dbl <= 10.0 ; dbl += 0.5) {
            myDoubleValues.add(dbl);
        }

        for (int i = 0 ; i < myDoubleValues.size() ; i++) {
            System.out.println(i + " --> " + myDoubleValues.get(i));
        }

    }
}
