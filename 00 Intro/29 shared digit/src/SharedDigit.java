public class SharedDigit {

    public static boolean hasSharedDigit(int digitOne, int digitTwo) {

        if(digitOne < 10 || digitOne > 99 || digitTwo < 10 || digitTwo > 99) return false;


        if(digitOne % 10 == digitTwo % 10 || digitOne % 10 == digitTwo /10 ||
           digitOne / 10 == digitTwo % 10 || digitOne / 10 == digitTwo /10) return true;
        return false;
    }
}
