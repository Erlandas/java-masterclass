public class NumberPalindrome {

    public static boolean isPalindrome(int number) {

        if(number < 0) number *= -1;

        int originalNumber = number;
        int reversedNumber = 0;

        while (number > 0) {
            reversedNumber = (reversedNumber + number % 10);
            if(number >= 10) {
                reversedNumber *= 10;
            }

            number /= 10;


        }

        if (originalNumber == reversedNumber) return true;
        return false;

    }
}
