package com.erlandas;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.*;

public class Main {

    public static void main(String[] args) {
        /*
        NOTE -->    If we try to copy file to already existing file
                    we will get Exception.
         */

        //--    To copy file
        try {
//            Path sourceFile = FileSystems.getDefault().getPath("Examples","file1.txt");
//            Path copyFile = FileSystems.getDefault().getPath("Examples","file1copy.txt");
//            //--    StandardCopyOption.REPLACE_EXISTING -> if file already exists replaces instead of throwing exception
//            Files.copy(sourceFile,copyFile, StandardCopyOption.REPLACE_EXISTING);

            //--    Copy directories
            //--    Copies just the folder not the files in it
            //--    If we want to copy with all the files
            //--    we have to walk directory tree in later videos will be EXAMPLE
//            sourceFile = FileSystems.getDefault().getPath("Examples","Dir1");
//            copyFile = FileSystems.getDefault().getPath("Examples","Dir4");
//            Files.copy(sourceFile,copyFile, StandardCopyOption.REPLACE_EXISTING);

            //********Move and rename a file

//            //MOVE
//            Path fileTOMove = FileSystems.getDefault().getPath("Examples", "file1copy.txt");
//            Path destination = FileSystems.getDefault().getPath("Examples","Dir1","file1copy.txt");
//            if(Files.exists(fileTOMove)) {
//                Files.move(fileTOMove,destination);
//            }
//
//            //RENAME
//            Path fileToRename = FileSystems.getDefault().getPath("Examples", "file1.txt");
//            Path destination2 = FileSystems.getDefault().getPath("Examples","file2.txt");
//            if(Files.exists(fileToRename)) {
//                System.out.println("'" + fileToRename.getFileName() + "' renamed to '" +destination2.getFileName() + "'" );
//                Files.move(fileToRename,destination2);
//            }

            //*********** To Delete a file

            Path fileToDelete = FileSystems.getDefault().getPath("Examples","Dir1","file1copy.txt");
            if(Files.exists(fileToDelete)) {
                Files.delete(fileToDelete);
            }
            //--> OR <--

            Files.deleteIfExists(fileToDelete);


        } catch (IOException e) {
            System.out.println("IOException : " + e.getMessage());
            e.printStackTrace();
        }


    }

//        Path path = FileSystems.getDefault().getPath("WorkingDirectoryFile.txt");
//        printFile(path);
//                                                // specify path within project directory
//        Path filePath = FileSystems.getDefault().getPath("files\\SubdirectoryFile.txt");
//        //--> OR <--                            // specify folder, then a file
//        filePath = FileSystems.getDefault().getPath("files","SubdirectoryFile.txt");
//        //--> OR <--
//        filePath = Paths.get(".","files","SubdirectoryFile.txt");
//        printFile(filePath);
//
//        Path filePathTwo = Paths.get("X:\\Java Masterclass\\07 Basic Input & Output including java.util\\OutThere.txt");
//        printFile(filePathTwo);
//
//        Path absolutePath = Paths.get(".");
//        //--    To print out an absolute path to the console
//        System.out.println(absolutePath.toAbsolutePath());
//
//
//        //--> Normalize path example good idea to NORMALIZE path always
//        Path path2 = FileSystems.getDefault().getPath(".", "files","..","files","SubdirectoryFile.txt");
//        System.out.println(path2.normalize().toAbsolutePath());
//        printFile(path2.normalize());
//
//        divider();
//        //*******Starts****Create a path to a file that doesn't exists
//        Path path3 = FileSystems.getDefault().getPath("thisfiledoesntexists.txt");
//        System.out.println(path3.toAbsolutePath());
//
//        divider();
//        Path path4 = Paths.get("X:\\","abcdef","doesntExists","whatever.txt");
//        System.out.println(path4.toAbsolutePath());
//
//        //******Check if path exists returns BOOLEAN
//        Path path5 = FileSystems.getDefault().getPath("files");
//        System.out.println("Path5 => Exists = " + Files.exists(path5));
//        System.out.println("path4 => Exists = " + Files.exists(path4));
//
//    }
//
//    //--    Method to read the file
//    private static void printFile(Path path) {
//        divider();
//        //--    Try with resources
//        try (BufferedReader fileReader = Files.newBufferedReader(path)) {
//
//            String line; //--   declare variable for line
//
//            //--    goes while is there still lines available to read and stores content of a line in to line variable
//            while ((line = fileReader.readLine()) != null) {
//                System.out.println(line);
//            }
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//            e.printStackTrace();
//        }
//        divider();
//    }

    private static void divider() {
        System.out.println("\n*******************************************************");
    }
}
