package com.erlandas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Theatre theatre = new Theatre("Olympian",8,12);

        //It makes shallow copy it means its seperate arrayLists but they refer to the same object
        List<Theatre.Seat> seatCopy = new ArrayList<>(theatre.seats);

        printList(seatCopy);

        seatCopy.get(1).reserve();

        if(theatre.reserveSeat("A02")) {
            System.out.println("Please pay for A02");
        } else {
            System.out.println("Seat already reserved");
        }

        Collections.shuffle(seatCopy);
        System.out.println("Printing seat copy");
        printList(seatCopy);

        System.out.println("Printing original theatre seats order");
        printList(theatre.seats);

        //This method MIN and MAX returns whole object
        Theatre.Seat minSeat = Collections.min(seatCopy);
        Theatre.Seat maxSeat = Collections.max(seatCopy);
        System.out.println("Min seat number is " + minSeat.getSeatNumber());
        System.out.println("Max seat number is " + maxSeat.getSeatNumber());


        //Testing implemented sort method
        sortList(seatCopy);
        System.out.println("Printing seat copy");
        printList(seatCopy);



    }

    public static void printList(List<Theatre.Seat> list) {
        for (Theatre.Seat seat: list) {
            System.out.print(" " + seat.getSeatNumber());
        }
        System.out.println();
        System.out.println("===========================================================================");
    }

    public static void sortList(List<? extends Theatre.Seat> list) {

        for(int i = 0 ; i < list.size()-1 ; i++) {

            for (int j = i+1 ; j < list.size() ; j++) {

                //If result is greater the 0 we want to swap them
                if(list.get(i).compareTo(list.get(j)) > 0) {

                    //SWAPS elements for us
                    Collections.swap(list,i,j);
                }
            }
        }
    }































}
