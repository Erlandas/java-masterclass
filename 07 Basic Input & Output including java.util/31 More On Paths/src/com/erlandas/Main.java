package com.erlandas;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {

        Path path = FileSystems.getDefault().getPath("WorkingDirectoryFile.txt");
        printFile(path);
                                                // specify path within project directory
        Path filePath = FileSystems.getDefault().getPath("files\\SubdirectoryFile.txt");
        //--> OR <--                            // specify folder, then a file
        filePath = FileSystems.getDefault().getPath("files","SubdirectoryFile.txt");
        //--> OR <--
        filePath = Paths.get(".","files","SubdirectoryFile.txt");
        printFile(filePath);

        Path filePathTwo = Paths.get("X:\\Java Masterclass\\07 Basic Input & Output including java.util\\OutThere.txt");
        printFile(filePathTwo);

        Path absolutePath = Paths.get(".");
        //--    To print out an absolute path to the console
        System.out.println(absolutePath.toAbsolutePath());


        //--> Normalize path example good idea to NORMALIZE path always
        Path path2 = FileSystems.getDefault().getPath(".", "files","..","files","SubdirectoryFile.txt");
        System.out.println(path2.normalize().toAbsolutePath());
        printFile(path2.normalize());

    }

    //--    Method to read the file
    private static void printFile(Path path) {
        divider();
        //--    Try with resources
        try (BufferedReader fileReader = Files.newBufferedReader(path)) {

            String line; //--   declare variable for line

            //--    goes while is there still lines available to read and stores content of a line in to line variable
            while ((line = fileReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        divider();
    }

    private static void divider() {
        System.out.println("\n*******************************************************");
    }
}
