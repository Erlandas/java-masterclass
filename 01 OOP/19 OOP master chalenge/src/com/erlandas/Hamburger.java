package com.erlandas;

public class Hamburger {
    private String name;
    private String breadRollType;
    private String meatType;
    private double price;
    private double totalPrice;

    private String addition1Name;
    private double addition1Price;

    private String addition2Name;
    private double addition2Price;

    private String addition3Name;
    private double addition3Price;

    private String addition4Name;
    private double addition4Price;

    public Hamburger(String name,String breadRollType,String meatType, double price) {
        this.name = name;
        this.breadRollType = breadRollType;
        this.meatType = meatType;
        this.price = price;

    }

    public void addAddition1(String name, double price) {

        this.addition1Name = name;
        this.addition1Price = price;
    }

    public void addAddition2(String name, double price) {

        this.addition2Name = name;
        this.addition2Price = price;
    }

    public void addAddition3(String name, double price) {

        this.addition3Name = name;
        this.addition3Price = price;
    }

    public void addAddition4(String name, double price) {

        this.addition4Name = name;
        this.addition4Price = price;
    }

    public double displayOrderTotalPrice() {
        double burgerPrice = this.price;
        if(addition1Name != null) {
            burgerPrice+=this.addition1Price;
            System.out.println("Added " + this.addition1Name + " for " + this.addition1Price + "$");
        }
        if(addition2Name != null) {
            burgerPrice+=this.addition2Price;
            System.out.println("Added " + this.addition2Name + " for " + this.addition2Price + "$");
        }
        if(addition3Name != null) {
            burgerPrice+=this.addition3Price;
            System.out.println("Added " + this.addition3Name + " for " + this.addition3Price + "$");
        }
        if(addition4Name != null) {
            burgerPrice+=this.addition4Price;
            System.out.println("Added " + this.addition4Name + " for " + this.addition4Price + "$");
        }
        return burgerPrice;
    }



}
