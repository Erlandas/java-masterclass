package com.erlandas;
/*
java.util.function package :
    That package contains functional interfaces that are meant to be used with lambda expressions.
    Interfaces aren't meant to do anything specific they represent the structure of lambda expressions
    that are commonly used in java.

Consumer :
    It's called consumer because --> object int nothing out.

Predicate :
    Condition passed through method
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Main {

    public static void main(String[] args) {
        Employee john = new Employee("John Doe", 30);
        Employee erlandas = new Employee("Erlandas Bacauskas", 31);
        Employee jack = new Employee("Jack Hill", 40);
        Employee snow = new Employee("Snow White",22);
        Employee red = new Employee("Red RidingHood", 35);
        Employee charming = new Employee("Prince Charming", 31);

        List<Employee> employees = new ArrayList<>();
        employees.add(john);
        employees.add(erlandas);
        employees.add(jack);
        employees.add(snow);
        employees.add(red);
        employees.add(charming);

        //Calling method with predicates
        printEmployeesByAge(employees,"Employees over 30",employee -> employee.getAge() > 30);
        printEmployeesByAge(employees,"\nEmployees 30 and younger",employee -> employee.getAge() <= 30);

        //--    Using predicate as anonymous class
        printEmployeesByAge(employees, "\nEmployees younger than 25", new Predicate<Employee>() {
            @Override
            public boolean test(Employee employee) {
                return employee.getAge() < 25;
            }
        });

        //--    IntPredicates This way we reusing predicates
        IntPredicate greaterThan15 = i -> i > 15;
        /*
            That's whats happening
            {
                int i;

                return i > 15;
            }
         */
        IntPredicate lessThan100 = i -> i < 100;
        //--    using greaterThan15 we calling test method and passing integer we want to test
        System.out.println(greaterThan15.test(10));
        int a = 20;
        System.out.println(greaterThan15.test(a + 5));

        //--    Chaining predicates
        System.out.println(greaterThan15.and(lessThan100).test(50));
        System.out.println(greaterThan15.and(lessThan100).test(15));

        //--    Scenario we want to print out 10 random numbers using SUPPLIER
        Random random = new Random();
        Supplier<Integer> randomSupplier = () -> random.nextInt(1000);
        for (int i = 0 ; i < 10 ; i++) {
            System.out.println(randomSupplier.get());
        }


    }

    //--    Method to print employees by age
    private static void printEmployeesByAge(List<Employee> employees,
                                            String ageText,
                                            //for loop passes each employee to the predicates test method
                                            //which in turn uses the employee as a parameter for the lambda expression
                                            //that maps to the predicate
                                            Predicate<Employee> ageCondition) {
        System.out.println(ageText);
        System.out.println("====================");

        for (Employee employee : employees) {
            if (ageCondition.test(employee)) {  //--    Test method tests condition with passed employee object
                System.out.println(employee.getName());
            }

        }
    }

}
