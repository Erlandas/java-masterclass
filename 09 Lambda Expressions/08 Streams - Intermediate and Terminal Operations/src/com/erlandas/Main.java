package com.erlandas;
/*
Streams in this context are completely unrelated to Input/Output streams

In this example when we refer to stream we refer to sequence of computations

Streams in this context ==> Set of computational steps that are chained together
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        List<String> someBingoNumbers = Arrays.asList(
                "N40","N36",
                "B12","B6",
                "G53","G49","G60","G50","g64",
                "I26","I17","I29",
                "O71");

        //--    List to store G numbers
        List<String> gNumbers = new ArrayList<>();

        /**
         *          OVERVIEW
         *
         *  Input                                   Method/Operation            Output
         *  ------------------------------------------------------------------------------------------------------------------------
         *  The ArrayList someBingoNumbers          stream()                    A stream that contains all the items in the
         *                                                                      someBingoNumbers
         *                                                                      list, in the same order
         *  ------------------------------------------------------------------------------------------------------------------------
         *  Stream containing all bingo numbers     map(String::toUpperCase)    A stream that contains all the bingo numbers
         *                                                                      upper cased
         *  ------------------------------------------------------------------------------------------------------------------------
         *  Upper cased stream                      filter(s->s.startsWith("G)  A stream containing all the items beginning with "G"
         *                                                                      ("G53","G49","G60","G50","G64")
         *  ------------------------------------------------------------------------------------------------------------------------
         *  "G" items stream                        sorted()                    A stream containing sorted items
         *                                                                      ("G49","G50","G53","G60","G64")
         *  ------------------------------------------------------------------------------------------------------------------------
         *  Sorted "G" items stream                 forEach                     Each "G" item is printed to the console. Void result.
         *                                              (System.out::println)   The chain ends.
         *  ========================================================================================================================
         *          When a chain is evaluated, a stream pipeline is created. The stream pipeline consists of a source, zero
         *          or more intermediate operations, and a terminal operation. In our example, we used a collection as the
         *          source, but we could also be an array or an I/O channel, and we can build streams from scratch.
         *
         *          We noticed that when we weren't using streams, our "g64" number was printed with a lower-cased 'g'.
         *          But when we used streams, it was printed with an upper-cased 'G'. In the non-stream case, we didn't
         *          use the result of the toUppercase() call. When an item passed the test, we assigned the original string
         *          with the lower-cased 'g' to the new list. (check previous project)
         *
         *          In the stream case, the map() method maps each source string to the Function result, therefore the
         *          upper-cased string is added to the resulting stream and passed to the next step in the chain. That's why
         *          the non-stream case prints a lower-cased 'g', and the stream case prints an upper-cased 'G'.
         */


        //--    Same code as below using Streams
        //--    EACH STREAM OPERATION OPERATES ON THE STREAM RESULT
        //      FROM THE LAST STEP
        someBingoNumbers
                .stream()
                // .map((String s) -> s.toUpperCase()) Or (String::toUpperCase)
                // :: -> this notation known as method reference
                // using this notation lambda calls existing method
                // In this case .map accepts function
                //      map() returns stream of all upper cased bingo numbers its essentially mapping
                //      each item in the input stream to the result returned by the function argument
                .map(String::toUpperCase)
                // after map() done we have a stream of upper case letters

                // next we want to filter the stream to have only those numbers that start with "G"
                // filter method once a predicate not a function so in this case we passing a
                // lambda expression that takes one parameter and returns True/False value
                // resulting in stream containing only those items that starts with "G"
                .filter(s->s.startsWith("G"))

                // .sorted() this method sorts based on the natural ordering of the items in the stream
                .sorted()

                // we printing out the results using forEach method
                // this forEach method not the same as we used previously with iterable interface
                // in this case we using forEach method from the stream class
                // -- the stream forEach method does what iterator method one does it accepts the consumer
                // as a parameter and evaluates the consumer for each item in the stream
                // -- since the (System.out::println) accepts an argument and doesn't return a value
                // -- we can map that to a consumer

                // forEach method is called terminal operation -> so terminal operation returns either void or
                // non stream result, since every operation in a stream requires source stream
                // ultimately the chain has to end when we use a terminal operation

                // Now operation that return a stream are called INTERMEDIATE OPERATIONS because they don't
                // force an end of a chain
                .forEach(System.out::println);

        //=======================================New Part Overview For TOP================================

        //in this case it will give stream of String objects
        //we can create a stream of any type of objects
        //but we cannot create stream of mixed types
        Stream<String> ioNumberStream = Stream.of("I26","I17","I29","071");
        Stream<String> inNumberStream = Stream.of("N40","N36","I26","I17","I29");

        //Now concatenate both streams together
        Stream<String> concatStream = Stream.concat(ioNumberStream,inNumberStream);

        //Now our stream contains duplicates
        //Now we remove duplicates and print number of elements ( Originally was 9 )

        System.out.println("----------------------------------------------------");
        System.out.println(concatStream
                .distinct()
                .peek(System.out::println)  // peek() lets print items after each stream mainly used for debugging
                .count());

        //To store sorted G numbers in a list
        System.out.println("----------------------------------------------------");
//        List<String> sortedGNumbers = someBingoNumbers
//                .stream()
//                .map(String::toUpperCase)
//                .filter(s -> s.startsWith("G"))
//                .sorted()
//                .collect(Collectors.toList());

        List<String> sortedGNumbers = someBingoNumbers
                .stream()
                .map(String::toUpperCase)
                .filter(s -> s.startsWith("G"))
                .sorted()
                //        Supplier      Accumulator     Combiner
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);

        for (String sortedGNumber : sortedGNumbers) {
            System.out.println(sortedGNumber);

        }





    }
}
