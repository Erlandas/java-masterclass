package com.erlandas;

import java.util.ArrayList;

public class MobilePhone {

    private String number;
    private ArrayList<Contacts> myContacts = new ArrayList<Contacts>();

    public MobilePhone(String number) {
        this.number = number;
        this.myContacts = new ArrayList<Contacts>();
    }

    public boolean addContact(Contacts contact) {
        if(findContact(contact.getContactName())>= 0) {
            System.out.println("Contact already exists");
            return false;
        }

        return true;
    }

    public boolean updateContact(Contacts oldContact, Contacts newContact) {
        int position = findContact(oldContact);
        if(position < 0) {
            System.out.println(oldContact.getContactName() + ", wasnt found");
            return false;
        }

        this.myContacts.set(position,newContact);
        return true;
    }

    private int findContact(Contacts contact) {
        return this.myContacts.indexOf(contact);
    }

    private int findContact(String contactName) {
        for (int i = 0 ; i < this.myContacts.size() ; i++) {
            if(this.myContacts.get(i).getContactName().equals(contactName)) {
                return i;
            }
        }

        return -1;
    }

    public String queryContact(Contacts contact) {
        if(findContact(contact) >= 0) {
            return contact.getContactName();
        }

        return null;
    }

    public boolean removeContact(Contacts contact) {
        int position = findContact(contact);
        if(position < 0) {
            System.out.println(contact.getContactName() + ", wasnt found");
            return false;
        }

        this.myContacts.remove(position);
        return true;
    }
}
