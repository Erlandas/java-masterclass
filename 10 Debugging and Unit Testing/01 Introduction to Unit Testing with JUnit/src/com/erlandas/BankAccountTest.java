package com.erlandas;

/*
To remove red markings
file -> project structure -> Modules -> beside JUnit at the left side from "Test" choose "Compile"
 */

import static junit.framework.TestCase.*;

public class BankAccountTest {

    @org.junit.Test
    public void deposit() {

        BankAccount account = new BankAccount("Erlandas","Bacauskas",1000.00, BankAccount.CHECKING);
        double balance = account.deposit(200.00,true);
        //third parameter "delta" as long as there is difference between expected and actual value
        //within delta its passing the test
        assertEquals(1200.00,balance,0);
    }

    @org.junit.Test
    public void withdraw() {
        fail("withdraw() This test has yet to be implemented");
    }

    @org.junit.Test
    public void getBalance_deposit() {
        BankAccount account = new BankAccount("Erlandas","Bacauskas",1000.00, BankAccount.CHECKING);
        account.deposit(200.00,true);
        assertEquals(1200,account.getBalance(),0);
    }

    @org.junit.Test
    public void getBalance_withdraw() {
        BankAccount account = new BankAccount("Erlandas","Bacauskas",1000.00, BankAccount.CHECKING);
        account.withdraw(200.00,true);
        assertEquals(800,account.getBalance(),0);
    }

    @org.junit.Test
    public void isChecking_true() {
        BankAccount account =
                new BankAccount("Erlandas","Bacauskas",1000.00, BankAccount.CHECKING);
        assertTrue(account.isChecking());
    }

    @org.junit.Test
    public void isChecking_false() {
        BankAccount account =
                new BankAccount("Erlandas","Bacauskas",1000.00, BankAccount.SAVINGS);
        assertFalse(account.isChecking());
    }

}