package com.erlandas;

import java.util.ArrayList;
import java.util.List;

public class Theatre {

    private final String theatreName;
    private List<Seat> seats = new ArrayList<>();

    public Theatre(String theatreName, int numRows, int seatsPerRow) {
        this.theatreName = theatreName;

        int lastRow = 'A' + (numRows-1);

        //Populate list with seat numbers
        for(char row = 'A' ; row <= lastRow; row++) {
            for(int seatNum = 1 ; seatNum <= seatsPerRow ; seatNum++) {
                Seat seat = new Seat(row + String.format("%02d",seatNum));
                seats.add(seat);
            }
        }
    }

    public String getTheatreName() {
        return theatreName;
    }

    //to reserve a seat
    public boolean reserveSeat(String seatNumber) {
        Seat requestedSeat = null;
        //1. Look for a seat
        for(Seat seat: seats) {
            if(seat.getSeatNumber().equals(seatNumber)) {
                requestedSeat = seat;
                break;
            }
        }

        //2. If seat is not found
        if(requestedSeat == null) {
            System.out.println("There is no seat " + seatNumber);
            return false;
        }

        //3. If seat is found pass to inner class to reserve method
        return requestedSeat.reserve();
    }

    //for testing
    public void getSeats() {
        for(Seat seat: seats) {
            System.out.println(seat.getSeatNumber());
        }
    }

    private class Seat {

        private final String seatNumber;
        private boolean reserved = false;

        public Seat(String seatNumber) {
            this.seatNumber = seatNumber;
        }

        //To reserve a seat
        public boolean reserve() {
            if(!this.reserved) {
                this.reserved = true;
                System.out.println("Seat " +seatNumber+ " reserved");
                return true;
            } else {
                return false;
            }
        }

        //to cancel reserved seat
        public boolean cancel() {
            if(this.reserved) {
                this.reserved = false;
                System.out.println("Reservation of seat " +seatNumber+ " canceled");
                return true;
            } else {
                return false;
            }
        }

        //Getter for seat number
        public String getSeatNumber() {
            return seatNumber;
        }
    }
}
