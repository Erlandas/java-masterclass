package com.erlandas;

import java.util.ArrayList;

public class Customer {
    private String customerName;
    private ArrayList<Double> transactions;

    public Customer(String customerName, double initialAmount) {
        this.customerName = customerName;

        transactions = new ArrayList<Double>();
        addTransaction(initialAmount);


    }

    public void addTransaction(double amount) {
        this.transactions.add(amount);
    }

    public String getCustomerName() {
        return customerName;
    }

    public ArrayList<Double> getTransactions() {
        return transactions;
    }
    //Optional if i want tocreate customer using method
    public Customer createCustomer(String name, Double amount) {
        return new Customer(name,amount);
    }
}
