package com.erlandas;

public class Main {

    public static void main(String[] args) {

        Animal dog = new Dog("Yorkie");
        dog.eat();
        dog.breathe();
        gap();
        Parrot bird = new Parrot("Parrot");
        bird.eat();
        bird.breathe();
        bird.fly();
        gap();

        Penguin penguin = new Penguin("Johny the penguin");
        penguin.fly();
        penguin.eat();
        penguin.breathe();
    }

    public static void gap() {
        System.out.println();
    }
}
