package com.erlandas.mylibrary;

public class Series {

    public static int nSum(int n) {
        return (n * (n + 1)) / 2;
    }

    public static int factorial(int n) {
        if(n == 0) {
            return 1;
        }
        int factorial = 1;


        for (int i = 1; i <= n ; i++) {
            factorial*=i;
        }
        return factorial;
    }

    public static long timsFibonacci(int f) {
        if(f == 0) return 0;
        else if(f == 1) return 1;

        long fibOne = 1;
        long fibTwo = 0;
        long fib = 0;

        for (int i = 1 ; i < f ; i++) {
            fib = fibOne + fibTwo;
            fibTwo = fibOne;
            fibOne = fib;
        }

        return fib;


    }
}

