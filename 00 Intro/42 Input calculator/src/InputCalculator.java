import java.util.Scanner;

public class InputCalculator {

    public static void inputThenPrintSumAndAverage() {
        Scanner scanner = new Scanner(System.in);

        int sum = 0;
        double avg = 0;
        int inputCounter = 0;

        while(true) {
            boolean isAnInt = scanner.hasNextInt();
            if(isAnInt){
                int number = scanner.nextInt();
                inputCounter++;
                sum += number;
                avg = (double)sum/ (double)inputCounter;
            } else {
                break;
            }
            scanner.nextLine();
        }

        long average = Math.round(avg);
        System.out.println("SUM = " +sum+ " AVG = " +average);

        scanner.close();

    }
}
