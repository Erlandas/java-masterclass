package com.erlandas;

public class Main {

    public static void main(String[] args) {
	// write your code here

        //To create case object first we have to create dimensions for the case
        Dimensions dimensions = new Dimensions(20,20,5);
        Case theCase = new Case("2020B", "Dell", "240", dimensions);

        //other way to create resolution object on the fly
        Monitor theMonitor = new Monitor("27inch Beast","Acer",27, new Resolution(2540,1440));

        Motherboard theMotherboard = new Motherboard("B3-200","Asus",4,6,"v2.44");

        PC thePC = new PC(theCase, theMonitor, theMotherboard);

        thePC.powerUp();



        //To access monitor functions first we reach PC then we get monitor from PC class and the we use methods from the monitor class
       /* thePC.getMonitor().drawPixelAt(1200,600,"red");

        thePC.getMotherboard().loadProgram("Windows 1.0");

        thePC.getTheCase().pressPowerButton();*/
    }
}
