package com.erlandas;

public class Car extends Vehicle{

    private double topSpeed;
    private int doorCount;
    private int sittingPlaces;
    private double engineSize;
    private String model;

    public Car(double weight, double wheelSize, int wheelCount, double topSpeed, int doorCount, int sittingPlaces, double engineSize, String model) {
        super(weight, wheelSize, wheelCount);
        this.topSpeed = topSpeed;
        this.doorCount = doorCount;
        this.sittingPlaces = sittingPlaces;
        this.engineSize = engineSize;
        this.model = model;
    }

    public void carStearing(String position) {
        switch(position) {
            case "left":
                System.out.println("Car moves left");
                break;
            case "right":
                System.out.println("Car moves right");
                break;
            case "reverse":
                System.out.println("Car reverses");
                break;
            case "front":
                System.out.println("Car moves ahead");
                break;
            default:
                System.out.println("Use stearing wheel");
        }
    }
}
