package com.erlandas;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static int[] baseData = new int[10];

    public static void main(String[] args) {

        System.out.println("Enter 10 integers :");
        getInput();
        printArray(baseData);
        resizeArray();

        System.out.println("Enter 12 integers :");
        getInput();
        printArray(baseData);
    }

    private static void getInput() {
        for(int i = 0 ; i < baseData.length ; i++)
            baseData[i] = scanner.nextInt();
    }

    private static void printArray(int[] array) {
        for(int i = 0 ; i < array.length ; i++)
            System.out.print(array[i] + " ");
        System.out.println();
    }

    //METHOD TO RESIZE ARRAY
    private static void resizeArray() {
        //1. make copy of original array
        int[] original = baseData;

        //2. initialize new size for existing array
        baseData = new int[12];

        //3. move elements from original array to resized array
        for (int i = 0 ; i < original.length ; i++)
            baseData[i] = original[i];
    }
}
