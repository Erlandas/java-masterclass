package com.erlandas;

public class StockItem implements Comparable<StockItem> {

    private final String name;
    private double price;
    private int quantityInStock; //Can be initialized later
    private int reserved = 0;

    //Constructors
    public StockItem(String name, double price) {
        this.name = name;
        this.price = price;
        this.quantityInStock = 0; //Or here (but you wouldn't normally do both)
    }

    public StockItem(String name, double price, int quantityStock) {
        this.name = name;
        this.price = price;
        this.quantityInStock = quantityStock;
    }

    //Getters
    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int availableQuantity() {
        return quantityInStock - reserved;
    }

    //Setters

    public void setPrice(double price) {
        //Check if price is greater the zero
        if(price > 0.0) {
            this.price = price;
        }
    }

    //Methods
    public void adjustStock(int quantity) {
        int newQuantity = this.quantityInStock + quantity;
        if(newQuantity >= 0) {
            this.quantityInStock = newQuantity;
        }
    }

    //+++++CHALLENGE PART+++++++
    //reserve stock
    public int reserveStock(int quantity) {
        if(quantity <= availableQuantity())  { //use the method not the field
            reserved+=quantity;
            return quantity;
        }

        return 0;
    }

    //unreserve
    public int unreserveStock(int quantity) {
        if(quantity <= reserved) {
            reserved -= quantity ;
            return quantity;
        }

        return 0;
    }

    //finalize stock
    public int finalizeStock(int quantity) {
        if(quantity <= reserved) {
            quantityInStock-=quantity;
            reserved -= quantity;
            return quantity;
        }

        return 0;
    }

    //Override equals()
    @Override
    public boolean equals(Object obj) {
       System.out.println("Entering StockItem.equals()");
        //If object is the same in memory checking if comparing same objects so its true
        if(obj == this) {
            return true;
        }

        //If object is null/empty or class is different return false
        if(obj == null || (obj.getClass() != this.getClass())) {
            return false;
        }

        String objName = ((StockItem) obj).getName();
        //compares equality by object name returns true if name is exactly the same and false if its not
        return this.name.equals(objName);
    }

    //Override hashCode()
    @Override
    public int hashCode() {
        return this.name.hashCode() + 31;
    }

    //Override compareTo()
    @Override
    public int compareTo(StockItem o) {
//        System.out.println("Entering StockItem.compareTo()");
        //this --> its the same object in memory equals to passed object o
        if(this == o) {
            return 0;
        }

        if(o != null) {
            //Sends object to inbuilt String compareTo method
            return this.name.compareTo(o.getName());
        }

        throw new NullPointerException();
    }

    //Override toString()
    @Override
    public String toString() {
        return this.name + " : price " + this.price + " reserved " + this.reserved;
    }

































}
