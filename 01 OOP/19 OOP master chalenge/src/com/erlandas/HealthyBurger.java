package com.erlandas;

public class HealthyBurger extends Hamburger {
    private String healthyExtra1Name;
    private double healthyExtra1price;

    private String healthyExtra2Name;
    private double healthyExtra2price;


    public HealthyBurger(String meatType, double price) {
        super("Healthy", "Rye Bread", meatType, price);

    }

    public void addHealthyExtra1(String name, double price) {
        this.healthyExtra1Name = name;
        this.healthyExtra1price = price;
    }

    public void addHealthyExtra2(String name, double price) {
        this.healthyExtra2Name = name;
        this.healthyExtra2price = price;
    }

    @Override
    public double displayOrderTotalPrice() {
        double burgerPrice = super.displayOrderTotalPrice();
        if(this.healthyExtra1Name != null) {
            burgerPrice+=this.healthyExtra1price;
            System.out.println("Added " +this.healthyExtra1Name+ " for " + this.healthyExtra1price+ "$");
        }
        if(this.healthyExtra2Name != null) {
            burgerPrice+=this.healthyExtra2price;
            System.out.println("Added " +this.healthyExtra2Name+ " for " + this.healthyExtra2price+ "$");
        }
        return burgerPrice;
    }
}
