package com.erlandas;

import java.util.ArrayList;

//Where is <T> we making this class generic
// <T extends Player> we saying that this class will take parameters only
//from player class and subclasses of a player class
// we making sure that other parameters wont be supplied to this class

public class Team<T extends Player> implements Comparable<Team<T>> {

    private String name;
    private int played = 0;
    private int won = 0;
    private int lost = 0;
    private int tied = 0;

    private ArrayList<T> members = new ArrayList<>();

    public Team(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean addPlayer(T player) {
        if(members.contains(player)) {
            System.out.println(player.getName() + "" +
                    " is already on a team");
            return false;
        }

        members.add(player);
        System.out.println(player.getName() + "" +
                " is added to a '" + this.name + "' team");
        return true;
    }

    public int numPlayer() {
        return this.members.size();
    }

    //<T> making sure can be checkt against only same sports teams
    public void matchResult(Team<T> oponnent, int ourScore, int theirScore) {

        String message;
        if(ourScore > theirScore) {
            message = " beat ";
            this.won++;
        } else if(ourScore == theirScore) {
            message = " drew with ";
            this.tied++;
        } else {
            message = " lost to ";
            this.lost++;
        }

        this.played++;
        //If passed as null it updates oponents results also
        if(oponnent != null) {
            System.out.println(this.getName() + message + oponnent.getName());
            oponnent.matchResult(null, theirScore, ourScore);
        }
    }

    public int ranking() {
        return (won * 2) + tied;
    }

    @Override
    public int compareTo(Team<T> team) {
        if(this.ranking() > team.ranking()) {
            return -1;
        } else if (this.ranking() < team.ranking()) {
            return 1;
        } else {
            return 0;
        }
    }
}
