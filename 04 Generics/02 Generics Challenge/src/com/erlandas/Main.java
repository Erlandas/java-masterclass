package com.erlandas;

import java.util.Collection;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {


        League<Team<FootballPlayer>> footballLeague = new League<>("AFL");

        Team<FootballPlayer> DublinMoorans = new Team<>("Dublin Moorans");
        Team<FootballPlayer> melbourne = new Team<>("Melbourne");
        Team<FootballPlayer> hawthorn= new Team<>("Hawthorn");
        Team<FootballPlayer> fremantle= new Team<>("Fremantle");

        //Team<BaseballPlayer> baseballTeam = new Team<>("Shit Heads");

        footballLeague.add(DublinMoorans);
        footballLeague.add(melbourne);
        footballLeague.add(hawthorn);
        footballLeague.add(fremantle);

        DublinMoorans.matchResult(melbourne,4,5);
        hawthorn.matchResult(fremantle,3,2);
        hawthorn.matchResult(fremantle,4,4);
        hawthorn.matchResult(fremantle,3,5);

        //footballLeague.add(baseballTeam);

        footballLeague.showLeagueTable();



    }
}
