package com.erlandas;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    private String albumName;
    private String albumArtist;
    private ArrayList<Song> songs;

    public Album(String albumName, String albumArtist) {
        this.albumName = albumName;
        this.albumArtist = albumArtist;
        this.songs = new ArrayList<Song>();
    }

    public String getAlbumName() {
        return albumName;
    }

    public String getAlbumArtist() {
        return albumArtist;
    }

    public ArrayList<Song> getSongs() {
        return songs;
    }

    //add song to the album
    public boolean addSong(String title, double duration) {
        if(findSong(title) == null) {
            this.songs.add(new Song(title,duration));
            return true;
        }
        return false;
    }


    //Find if song exists or no in the album
    private Song findSong(String title) {
        for (int i = 0 ; i < songs.size() ; i++) {
            Song currentSong = this.songs.get(i);
            if(currentSong.getTitle().equals(title)) {
                return currentSong;
            }
        }
        return null;
    }

    //Add song by tract number PLAYLIST
    public boolean addToPlayList(int trackIndex, LinkedList<Song> playlist) {

        if(trackIndex-1 >= 0 && trackIndex-1 <= this.songs.size()) {
            playlist.add(this.songs.get(trackIndex-1));
            System.out.println("Added");
            return true;
        }
        System.out.println("NOT Added");
        return false;
    }

    //Add song by title PLAYLIST
    public boolean addToPlayList(String songTitle, LinkedList<Song> playlist) {
        Song checkedSong = findSong(songTitle);
        if(checkedSong != null) {
            playlist. add(checkedSong);
            System.out.println("Added");
            return true;
        }

        System.out.println("NOT Added");
        return false;
    }
























}
