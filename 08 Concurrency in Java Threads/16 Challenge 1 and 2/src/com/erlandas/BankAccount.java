package com.erlandas;

public class BankAccount {

    private double balance;
    private String accountNumber;

    public BankAccount(String accountNumber, double initialBalance) {
        this.accountNumber = accountNumber;
        this.balance = initialBalance;
        System.out.println("Bank account created " + this.accountNumber +
                " with initial balance of " + this.balance);
    }

    //Challenge #2

    //--    Way One
//    public synchronized void deposit(double amount) {
//        balance += amount;
//        System.out.println("Deposit of " + amount + " made, current balance " + this.balance);
//    }
//
//    public synchronized void withdraw(double amount) {
//        balance -= amount;
//        System.out.println("Withdraw of " + amount + " made, current balance " + this.balance);
//    }

    //--    Way Two
    public void deposit(double amount) {
        synchronized (this) {
            balance += amount;
            System.out.println("Deposit of " + amount + " made, current balance " + this.balance);
        }

    }

    public void withdraw(double amount) {
        synchronized (this) {
            balance -= amount;
            System.out.println("Withdraw of " + amount + " made, current balance " + this.balance);
        }

    }
}
