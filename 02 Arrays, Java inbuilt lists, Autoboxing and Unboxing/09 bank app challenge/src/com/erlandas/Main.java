package com.erlandas;

public class Main {

    public static void main(String[] args) {

        Bank bank = new Bank("Irish Bank");

        bank.addBranch("AIB");

        bank.addCustomerToBranch("AIB","Erlandas",203.5);
        bank.addCustomerToBranch("AIB","Kornelija", 1100.99);
        bank.addCustomerToBranch("AIB","John Smith", 99.9);

        bank.addCustomerTransaction("AIB","Erlandas",23);
        bank.addCustomerTransaction("AIB","Erlandas",123);



        if(!bank.addCustomerToBranch("AIB","John",123)) {
            System.out.println("Error branch does not exists");
        }
        bank.listCustomers("AIB", true);

        if (!bank.addCustomerToBranch("AIB","Erlandas",123.3)) {
            System.out.println("Customer already exists");
        }
    }
}
