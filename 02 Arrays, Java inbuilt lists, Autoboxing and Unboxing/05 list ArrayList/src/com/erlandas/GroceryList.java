package com.erlandas;

import java.util.ArrayList;

public class GroceryList {

    private ArrayList<String> groceryList = new ArrayList<>();

    //getter
    public ArrayList<String> getGroceryList() {
        return groceryList;
    }

    //Add item to a grocery list
    public void addGroceryItem(String item) {
        groceryList.add(item);
    }

    //Print arrayList
    public void printGroceryList() {
        System.out.println("You have " +groceryList.size()+ " items in a list");
        for (int i = 0 ; i < groceryList.size() ; i++) {
            System.out.println((i+1) + ". " + groceryList.get(i));
        }
    }


    //replace item one parameter
    public void modifyGroceryList(String currentItem,String newItem) {
        //modifyGroceryList(groceryList.indexOf(newItem), newItem);
        if(findItem(currentItem)>=0) {
            modifyGroceryList(findItem(currentItem),newItem);
        }

    }


    //replace existing item internal method
    private void modifyGroceryList(int position, String newItem) {
        groceryList.set(position, newItem);
    }


    //remove grocery item one parameter
    public void removeGroceryItem(String item) {
        if(findItem(item)>=0) {
            removeGroceryItem(findItem(item));
        }
    }

    //remove item from arrayList internal method
    private void removeGroceryItem(int position) {
        groceryList.remove(position);
    }

    //find item in arrayList
    private int findItem(String searchItem) {

        return groceryList.indexOf(searchItem);
        //Returns boolean if item exists in list
        //boolean exists = groceryList.contains(searchItem);

       /* int position = groceryList.indexOf(searchItem);
        if(position >= 0) {
            return groceryList.get(position);
        }

        return null;*/
    }

    public boolean onFile(String searchItem) {
        if(findItem(searchItem) >= 0) {
            return true;
        }

        return false;
    }
}
