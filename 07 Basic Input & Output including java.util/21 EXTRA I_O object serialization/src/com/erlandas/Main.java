package com.erlandas;

import java.io.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        //***********OBJECT SERIALIZATION***********

        /*
        1.  Object serialization is the process of converting an object
            into a series of bytes so they can be written to disk.

        2.  Object to Disk -> Serialization

        3.  Disk to Object -> Deserialization

        4.  To add this capability to your Java Class, simply
            implement Serializable in the class definition

            Example:
                public class Student implements Serializable {/..../}
         */

        //*********OBJECT SERIALIZATION FILE I/O - JAVA CLASSES*****

        /*
        1.  This type of I/O uses streams of a raw bytes. Contents of a files
            are in a binary format.

        2.  FileInputStream / FileOutputStream - Used to
            read/write to a file, treating it as bytes instead of a text.

        3.  ObjectInputStream / ObjectOutputStream - Used to
            deserialize/serialize a data input stream back into an object.

         */

        File file = new File("students.txt");
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student("Tom", 3.921));
        students.add(new Student("Dave", 4.0));
        students.add(new Student("Bill", 2.0));

        //********Serialize the collection of students***********

        //File Output Stream takes the file
        FileOutputStream fileOutput = new FileOutputStream(file);

        //Object Output Stream takes the File Output Stream
        ObjectOutputStream output = new ObjectOutputStream(fileOutput);

        //Write each entry from list to a file
        for (Student s : students) {

            output.writeObject(s);

        }

        //Close after done
        output.close();
        fileOutput.close();

        //*********Deserialize the file back into the collection************

        //File Input Stream takes the file
        FileInputStream fileInput = new FileInputStream(file);

        //Object Input Stream takes the File Input Stream
        ObjectInputStream input = new ObjectInputStream(fileInput);

        ArrayList<Student> studentsTwo = new ArrayList<>();

        try {
            while (true) {
            /*
            When we read the object it returns the object back
            we have to cast the object in to a type student
             */

                Student s = (Student) input.readObject();
                studentsTwo.add(s);
            }

            //We have to catch end of file exception when all file is finished to read
        } catch (EOFException e) {
            System.out.println("End of file reached");
        }

        //Print out newly created collection of studentsTwo
        for (Student s : studentsTwo) {
            System.out.println(s.toString());
        }
    }
}
