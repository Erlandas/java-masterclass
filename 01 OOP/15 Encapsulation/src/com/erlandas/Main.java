package com.erlandas;

public class Main {

    public static void main(String[] args) {
	// write your code here

        //NO ENCAPSULATION CODE
        /*
        NoEncapsulationPlayer player = new NoEncapsulationPlayer();
        player.name = "Erlandas";
        player.health = 20;
        player.weapon = "Empty beer bottle";

        int damage = 10;
        player.looseHealth(damage);
        System.out.println("Remaining health " +player.getHealth());

        //when we not using encapsulation we can override variable values
        // thats bad as we can bend the rules of the game aka cheating
        damage = 11;
        player.health = 200;
        player.looseHealth(damage);
        System.out.println("Remaining health " +player.getHealth()); */

        //ENCAPSULATION
        EncapsulatedPlayer playerE = new EncapsulatedPlayer("Erlandas",50,"Katana");
        System.out.println("Initial health " + playerE.getHealth());
    }
}
