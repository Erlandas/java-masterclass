package com.erlandas;

public class Printer {
    private double tonerLevel ;
    private double pagesPrinted1 = 0;
    private double pagesPrinted2 = 0;
    private boolean duplex;

    public Printer(double tonerLevel, boolean duplex) {

        if(tonerLevel >= 0 && tonerLevel <= 100) {
            this.tonerLevel = tonerLevel;
        } else {
            this.tonerLevel = -1;
        }
        this.duplex = duplex;
    }

    public void fillTonerLevel(double tonerAmount) {
        if(this.tonerLevel + tonerAmount > 100) {
            this.tonerLevel = 100.0;
        } else {
            this.tonerLevel = this.tonerLevel + tonerAmount;
        }

        System.out.println("Toner level filled up to " + this.tonerLevel + "%");
    }

    public void printPages(boolean twoSided, int amountOfPages) {
        if(twoSided && this.duplex && this.tonerLevel > 0) {
            this.pagesPrinted2 = this.pagesPrinted2 + amountOfPages;
            this.tonerLevel = this.tonerLevel - 0.2 * amountOfPages;
            System.out.println("Pages printed on two sides");
            System.out.println("Current toner level --> " + this.tonerLevel+ "%");
            System.out.println("Total pages printed two sides --> " + (int)(this.pagesPrinted2));
        } else if (!twoSided && this.tonerLevel > 0) {
            this.pagesPrinted1 = this.pagesPrinted1 + amountOfPages;
            this.tonerLevel = this.tonerLevel - 0.1 * amountOfPages;
            System.out.println("Pages printed on one side");
            System.out.println("Current toner level --> " + this.tonerLevel+ "%");
            System.out.println("Total pages printed one side --> " + (int)(this.pagesPrinted1));
        } else {
            System.out.println("Something went wrong");
        }
    }

    public void printouts() {
        System.out.println("One side printed --> " + pagesPrinted1);
        System.out.println("Two side printed --> " + pagesPrinted2);
    }


}
