package com.erlandas;

/*
To remove red markings
file -> project structure -> Modules -> beside JUnit at the left side from "Test" choose "Compile"
 */

import org.junit.AfterClass;

import static junit.framework.TestCase.*;

public class BankAccountTest {

    private BankAccount account;
    private static int count;

    @org.junit.BeforeClass
    public static void beforeClass() {
        System.out.println("This executes before any test cases. Count = " + count++);
    }

    @org.junit.Before
    public void setup() {
        //--    this objects instance created before running every test
        //--    avoiding creating instances of object in every test
        account = new BankAccount("Erlandas","Bacauskas",1000.00, BankAccount.CHECKING);
        System.out.println("Running a test");
    }

    @org.junit.Test
    public void deposit() {
        double balance = account.deposit(200.00,true);
        assertEquals(1200.00,balance,0);
    }

    @org.junit.Test
    public void withdraw_branch() {
        double balance = account.withdraw(600,true);
        assertEquals(400.00,balance,0);
    }

    //--    if exception is expected
    @org.junit.Test(expected = IllegalArgumentException.class)
    public void withdraw_notBranch() {
        double balance = account.withdraw(600,false);
        assertEquals(400.00,balance,0);
    }

    @org.junit.Test
    public void getBalance_deposit() {
        account.deposit(200.00,true);
        assertEquals(1200,account.getBalance(),0);
    }

    @org.junit.Test
    public void getBalance_withdraw() {
        account.withdraw(200.00,true);
        assertEquals(800,account.getBalance(),0);
    }

    @org.junit.Test
    public void isChecking_true() {
        assertTrue(account.isChecking());
    }

    @org.junit.AfterClass
    public static void afterClass() {
        System.out.println("This executes after test cases. Count = " + count++);
    }

    @org.junit.After
    public void tearDown() {
        System.out.println("Count = " + count++);
    }
}