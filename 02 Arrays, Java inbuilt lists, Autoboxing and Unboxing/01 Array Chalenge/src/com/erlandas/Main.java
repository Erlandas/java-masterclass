package com.erlandas;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int[] integerArray = getIntegers(5);

        printArray(integerArray);

        //TO COPY AN ARRAY WITH INBUILT FUNCTION

        int[] copyOfArray = Arrays.copyOf(integerArray,integerArray.length);

    }

    public static int[] getIntegers(int number) {
        int[] array = new int[number];
        System.out.println("Enter " +number+ " integer values\r");
        for (int i = 0 ; i < array.length ; i++) {
            array[i] = scanner.nextInt();
        }

        array = sortArray(array);
        return array;
    }

    //My solution for sorting

    public static int[] sortedArray(int[] array) {

        for (int j = 0 ;j < array.length-1; j++ ) {
            for (int i = 0 ; i < array.length-1; i++) {
                int temp = array[i];
                if(temp < array[i+1]) {
                    array[i] = array[i+1];
                    array[i+1] = temp;
                }
            }
        }

        return array;
    }

    //Tims solution for sorting

    public static int[] sortArray(int[] array) {
        boolean flag = true;
        int temp;

        while (flag) {
            flag = false;
            for(int i = 0 ; i < array.length-1 ; i++) {
                if(array[i] < array[i+1]) {
                    temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    flag = true;
                }
            }
        }

        return array;
    }

    public static void printArray(int[] array) {
        System.out.println("Sorted array");
        for (int i = 0 ; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }
}
