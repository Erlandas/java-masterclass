package com.erlandas;

import java.util.Random;

public class Writer implements Runnable {

    private Message message;

    public Writer(Message message) {

        this.message = message;

    }

    public void run() {
        String[] mmessages = {
                "Humpty Dumpty sat on a wall",
                "Humpty Dumpty had a great fall",
                "All the king's horses and all the king's men",
                "Couldn't put Humpty together again"
        };

        Random random = new Random();

        for (int i = 0 ; i < mmessages.length ; i++) {
            message.write(mmessages[i]);
            try {
                Thread.sleep(random.nextInt(2000));
            } catch (InterruptedException e) {

            }
        }
        message.write("Finished");
    }

}
