package com.company;

public class Main {

    public static void main(String[] args) {

        // int has a width of 32
        //we can use underscores on large numbers from java 7 to make numbers more readable
        int minValueForInteger = -2_147_483_648; // smallest number that can fit in integer
        int maxValueForInteger = 2_147_483_647; // largest number that can fit in integer

        // byte has a width of 8
        //Byte is more eficient as it takes smaller amount of space
        byte minByteValue = -128;
        byte maxByteValue = 127;

        // short has a width of 16
        short minShortValue =  -32768;
        short maxShortValue = 32767;

        // Long has a width of 64
        // declaring with l or L at the end of a number max val 2^63
        long myLongValue = 100L;
        System.out.println(Math.pow(2,63));

        //NOTE: for int and long data types we dont need to do casting

    }
}
