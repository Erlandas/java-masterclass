package com.erlandas;

public class Main {

    public static void main(String[] args) {

        Customer customer = new Customer("Erlandas",99.99);
        Customer anotherCustomer;
        //TODO this way its pointing to a memory location where customer object is
        anotherCustomer = customer;
        //TODO if changes made it will affect first customer
        anotherCustomer.setBalance(11.11);

        System.out.println("Balance for customer " + customer.getName() + " is " + customer.getBalance());
    }
}
