package com.erlandas;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Dimensions windowDimensions = new Dimensions(1,2);
        Dimensions doorDimensions = new Dimensions(2,1);

        Bedroom theBedroom = new Bedroom("King","Blackout","Electric",windowDimensions,doorDimensions);

        House theHouse = new House(theBedroom);

        theHouse.getBedroom().useShower();
        theHouse.getBedroom().closeCurtains();
        theHouse.getBedroom().lockDorrs();
        theHouse.sexTime();
    }
}
