package com.erlandas;

/*
Challenge #1 :
    We could have two people using a joint bank account at the same time. Create
    and start two threads that use the same BankAccount instance and an initial
    balance of $1000.00. One will deposit $300.00 into the bank account, and then
    withdraw $50.00. The other will deposit $203.75 and then withdraw $100.00.

Challenge #2 :
    Make the BankAccount class Threadsafe using the synchronize keyword

Challenge #3 :
    Make the BankAccount class threadsafe again using the synchronized keyword

    We've added two new methods(getAccountNumber, printAccountNumber) in the BankAccount
    class. Update the code so that the BankAccount class is threadsafe.

Challenge #3 note:
    It was a trick. We don't have to synchronize any extra code. Because both threads only
    read the account number, thread interference isn't an issue. If we over-synchronize the
    code,it will have impact on performance in large applications.

Challenge #4 :
    Use ReentrantLock

    Instead of using synchronized keyword, make the bankAccount class threadsafe using
    the ReentrantLock class.

Challenge #5 :
    Use tryLock with a timeout value

    Hint: Use the form of the tryLock() method that accepts two parameters - the first
    parameter is the timeout value, and the second parameter is the time unit of the first
    parameter. use TimeUnit.MILLISECONDS for the second parameter.

Challenge #6 :
    Update the code so that the status variable is thread safe

    Trick: No changes made. Since the status variable is local variable its already threadsafe.
    Local variables are stored on the thread stack, so each thread will have its own copy.

 */

public class Main {

    public static void main(String[] args) {

        BankAccount account = new BankAccount("12345-678", 1000.00);

        //Challenge #1
        Thread TrThread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                account.deposit(300.00);
                account.withdraw(50.00);
            }
        });

        Thread TrThread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                account.deposit(203.75);
                account.withdraw(100.00);
            }
        });

        TrThread1.start();
        TrThread2.start();

    }
}
