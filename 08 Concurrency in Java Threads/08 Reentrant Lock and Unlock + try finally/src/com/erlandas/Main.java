package com.erlandas;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

    public static final String EOF = "EOF";

    public static void main(String[] args) {
        /*
        Problem happens when all the threads wants to access same list at the same time
        thats why we synchronizing in our code list in the other classes
        to allow threads to complete their tasks before passing to another thread
         */

        List<String> buffer = new ArrayList<>();

        ReentrantLock bufferLock = new ReentrantLock();

        MyProducer producer = new MyProducer(buffer,ThreadColor.ANSI_YELLOW, bufferLock);
        MyConsumer consumer1 = new MyConsumer(buffer, ThreadColor.ANSI_PURPLE, bufferLock);
        MyConsumer consumer2 = new MyConsumer(buffer, ThreadColor.ANSI_CYAN, bufferLock);

        new Thread(producer).start();
        new Thread(consumer1).start();
        new Thread(consumer2).start();





    }
}
