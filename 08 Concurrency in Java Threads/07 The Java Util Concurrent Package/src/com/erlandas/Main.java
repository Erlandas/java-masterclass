package com.erlandas;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static final String EOF = "EOF";

    public static void main(String[] args) {
        /*
        Problem happens when all the threads wants to access same list at the same time
        thats why we synchronizing in our code list in the other classes
        to allow threads to complete their tasks before passing to another thread
         */

        List<String> buffer = new ArrayList<>();
        MyProducer producer = new MyProducer(buffer,ThreadColor.ANSI_YELLOW);
        MyConsumer consumer1 = new MyConsumer(buffer, ThreadColor.ANSI_PURPLE);
        MyConsumer consumer2 = new MyConsumer(buffer, ThreadColor.ANSI_CYAN);

        new Thread(producer).start();
        new Thread(consumer1).start();
        new Thread(consumer2).start();





    }
}
