package com.erlandas;


public class Main {
    //Create objects it will be holding our locks
    public static Object lock1 = new Object();
    public static Object lock2 = new Object();

    public static void main(String[] args) {
	/*
	Deadlock occurs when two or more threads are blocked on locks
	and every thread that's blocked are holding a lock that another blocked thread wants

	Example:
	    Thread 1 is holding lock 1 and waiting for lock 2
	    Thread 2 is holding lock 2 and waiting for lock 1

	    Because all the threads are holding locks are blocked they never release the locks they holding
	    and so none of the waiting threads will actually ever run
	 */

	new Thread1().start();
	new Thread2().start();

    }

    private static class Thread1 extends Thread {
        public void run() {
            synchronized (lock1) {
                System.out.println("Thread 1: has lock 1");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {

                }
                System.out.println("Thread 1: waiting for lock 2");
                synchronized (lock2) {
                    System.out.println("Thread 1: has lock 1 and lock 2");
                }
                System.out.println("Thread 1: released lock 2");
            }
            System.out.println("Thread 1: Released lock 1. Exiting...");
        }
    }

    //Makes DEADLOCK TO OCCUR
    /*
    private static class Thread2 extends Thread {
        public void run() {
            synchronized (lock2) {
                System.out.println("Thread 2: has lock 2");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {

                }
                System.out.println("Thread 2: waiting for lock 1");
                synchronized (lock1) {
                    System.out.println("Thread 2: has lock 2 and lock 1");
                }
                System.out.println("Thread 2: released lock 1");
            }
            System.out.println("Thread 2: Released lock 2. Exiting...");
        }
    }
     */

    //Fixes DEADLOCK issue
    private static class Thread2 extends Thread {
        public void run() {
            synchronized (lock1) {
                System.out.println("Thread 2: has lock 1");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {

                }
                System.out.println("Thread 2: waiting for lock 2");
                synchronized (lock2) {
                    System.out.println("Thread 2: has lock 1 and lock 2");
                }
                System.out.println("Thread 2: released lock 2");
            }
            System.out.println("Thread 2: Released lock 1. Exiting...");
        }
    }
}


