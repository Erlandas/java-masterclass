package com.erlandas;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    //Methods declared in main abstract class must be implemented in subclasses
    @Override
    public void eat() {
        System.out.println(getName() + " Breathing");
    }

    @Override
    public void breathe() {
        System.out.println("Breathe in, breathe out, repeat....");
    }
}
