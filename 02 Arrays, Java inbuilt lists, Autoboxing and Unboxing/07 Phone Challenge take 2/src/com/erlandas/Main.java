package com.erlandas;


import java.util.Scanner;

public class Main {

    public static Scanner scanner = new Scanner(System.in);

    public static MobilePhone phone = new MobilePhone();
    public static void main(String[] args) {




        boolean terminate = false;

        printPhoneMenu();

        while(!terminate) {
            System.out.print("\tEnter option : ");
            int option = scanner.nextInt();
            scanner.nextLine();

            switch(option) {
                case 1:
                    printPhoneMenu();
                    break;
                case 2:
                    phone.printContacts();
                    break;
                case 3:
                    addNewContact();
                    break;
                case 4:
                    updateContact();
                    break;
                case 5:
                    removeContact();
                    break;
                case 6:
                    System.out.println("\tTurning OFF");
                    terminate = true;
                    break;
            }

        }

    }

    public static void removeContact() {
        System.out.println("\n\tEnter contact to replace");
        System.out.print("\tName : ");
        String oldName = scanner.nextLine();
        Contact existing = phone.queryContact(oldName);
        if (existing == null) {
            System.out.println("Contact not found");
            return;
        }
        phone.removeContact(existing);
    }

    public static void updateContact() {
        System.out.println("\n\tEnter contact to replace");
        System.out.print("\tName : ");
        String oldName = scanner.nextLine();
        Contact existing = phone.queryContact(oldName);
        if(existing == null) {
            System.out.println("Contact not found");
            return;
        }
        System.out.println("\n\tEnter new contact");
        System.out.print("\tName : ");
        String newName = scanner.nextLine();
        System.out.print("\tNumber : ");
        String newNumber = scanner.nextLine();

        Contact newContact = Contact.createContact(newName,newNumber);

        phone.updateContact(existing,newContact);

    }

    public static void printPhoneMenu() {
        System.out.println("\t1.\tPrint menu options");
        System.out.println("\t2.\tPrint contact list");
        System.out.println("\t3.\tAdd new contact");
        System.out.println("\t4.\tUpdate contact");
        System.out.println("\t5.\tRemove contact");
        System.out.println("\t6.\tTurn OFF");
    }

    public static void addNewContact() {
        System.out.print("\n\tEnter contact name : ");
        String name = scanner.nextLine();
        System.out.print("\tEnter " +name+ " number : ");
        String number = scanner.nextLine();
        Contact contact = Contact.createContact(name,number);
        phone.addContact(contact);
    }
}
