package com.erlandas;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class Main {

    public static void main(String[] args) {

        try {

            //--> To Create a File

            Path fileToCreate = FileSystems.getDefault().getPath("Examples" , "file2.txt");
            if(!Files.exists(fileToCreate)) {
                System.out.println("File '" + fileToCreate.getFileName() + "' created");
                Files.createFile(fileToCreate);
            } else {
                System.out.println("File '" + fileToCreate.getFileName() + "' already exists");
            }


            //--> To Create a Directory

            Path dirToCreate = FileSystems.getDefault().getPath("Examples","Dir4");
            if(!Files.exists(dirToCreate)) {
                System.out.println("Directory '" + dirToCreate.getFileName() + "' created");
                Files.createDirectory(dirToCreate);
            } else {
                System.out.println("Directory '" + dirToCreate.getFileName() + "' already exists");
            }

            //--> Create Directories Tree

            Path directoriesToCreate = FileSystems.getDefault().getPath("Examples","Dir2\\Dir3\\Dir4\\Dir5\\Dir6");
            if(!Files.exists(directoriesToCreate)) {
                System.out.println("Directories tree '" + directoriesToCreate.toAbsolutePath() + "' created");
                Files.createDirectories(directoriesToCreate);
            } else {
                System.out.println("Directories tree '" + directoriesToCreate.toAbsolutePath() + "' already exists");
            }

            //--> Get File Size
            Path filePath = FileSystems.getDefault().getPath("Examples","Dir1","file1.txt");
            long fileSize = Files.size(filePath);
            System.out.println("'" + filePath.getFileName() + "' file size is " + fileSize);
            System.out.println("'" + filePath.getFileName() + "' file last modified " + Files.getLastModifiedTime(filePath));


            //--> File Attributes
            BasicFileAttributes attrs = Files.readAttributes(filePath,BasicFileAttributes.class);
            System.out.println("Size = " + attrs.size());
            System.out.println("Last modified = " + attrs.lastModifiedTime());
            System.out.println("Created = " + attrs.creationTime());
            System.out.println("Is directory = " + attrs.isDirectory());
            System.out.println("Is regular file = " + attrs.isRegularFile());


        } catch (IOException e) {
            System.out.println("IOException : " + e.getMessage());
            e.printStackTrace();
        }


    }

//        Path path = FileSystems.getDefault().getPath("WorkingDirectoryFile.txt");
//        printFile(path);
//                                                // specify path within project directory
//        Path filePath = FileSystems.getDefault().getPath("files\\SubdirectoryFile.txt");
//        //--> OR <--                            // specify folder, then a file
//        filePath = FileSystems.getDefault().getPath("files","SubdirectoryFile.txt");
//        //--> OR <--
//        filePath = Paths.get(".","files","SubdirectoryFile.txt");
//        printFile(filePath);
//
//        Path filePathTwo = Paths.get("X:\\Java Masterclass\\07 Basic Input & Output including java.util\\OutThere.txt");
//        printFile(filePathTwo);
//
//        Path absolutePath = Paths.get(".");
//        //--    To print out an absolute path to the console
//        System.out.println(absolutePath.toAbsolutePath());
//
//
//        //--> Normalize path example good idea to NORMALIZE path always
//        Path path2 = FileSystems.getDefault().getPath(".", "files","..","files","SubdirectoryFile.txt");
//        System.out.println(path2.normalize().toAbsolutePath());
//        printFile(path2.normalize());
//
//        divider();
//        //*******Starts****Create a path to a file that doesn't exists
//        Path path3 = FileSystems.getDefault().getPath("thisfiledoesntexists.txt");
//        System.out.println(path3.toAbsolutePath());
//
//        divider();
//        Path path4 = Paths.get("X:\\","abcdef","doesntExists","whatever.txt");
//        System.out.println(path4.toAbsolutePath());
//
//        //******Check if path exists returns BOOLEAN
//        Path path5 = FileSystems.getDefault().getPath("files");
//        System.out.println("Path5 => Exists = " + Files.exists(path5));
//        System.out.println("path4 => Exists = " + Files.exists(path4));
//
//    }
//
//    //--    Method to read the file
//    private static void printFile(Path path) {
//        divider();
//        //--    Try with resources
//        try (BufferedReader fileReader = Files.newBufferedReader(path)) {
//
//            String line; //--   declare variable for line
//
//            //--    goes while is there still lines available to read and stores content of a line in to line variable
//            while ((line = fileReader.readLine()) != null) {
//                System.out.println(line);
//            }
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//            e.printStackTrace();
//        }
//        divider();
//    }

    private static void divider() {
        System.out.println("\n*******************************************************");
    }
}
