package com.erlandas;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//        int x = 98;
//        int y = 0;
//
//        System.out.println(divideLBYL(x,y));
//        System.out.println(divideEAFP(x,y));
//
//        //Below will crash no exception handeling
//        //System.out.println(divide(x,y));

        //int x = getInt();
        //int x = getIntLBYL();
        int x = getIntEAFP();
        System.out.println("x is " + x);

    }

    //Handling input
    private static int getInt() {
        Scanner s = new Scanner(System.in);
        return s.nextInt();
    }

    //Look before you leave handling input LBYL
    private static int getIntLBYL() {
        Scanner s = new Scanner(System.in);
        boolean isValid = true;
        System.out.println("Please enter an integer ");
        String input = s.next();
        for (int i = 0 ; i < input.length() ; i++) {
            if(!Character.isDigit(input.charAt(i))) {
                isValid = false;
                break;
            }
        }

        if(isValid) {
            return Integer.parseInt(input);
        }

        return 0;
    }

    //Using EAFP for input
    private static int getIntEAFP() {
        Scanner s = new Scanner(System.in);

        System.out.println("Please enter integer ");
        try {
            return s.nextInt();
        } catch (InputMismatchException e) {
            return 0;
        }
    }

    //LBYL -> Look Before You Leave example
    //Checks first
    private static int divideLBYL(int x, int y) {

        if(y != 0) {
            return x / y;
        } else {
            return 0;
        }

    }

    //EAFP -> Ask For Forgivenes And Permission
    private static int divideEAFP(int x, int y) {

        try {
            return x/y;
        } catch (ArithmeticException e) {
            //System.out.println(e);
            return 0;
        }
    }

    private static int divide(int x,int y) {
        return x/y;
    }
}
