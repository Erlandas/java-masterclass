package com.erlandas;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int[] arrayToTest = {1,2,3,4,5,6};

        System.out.println("Original passed : " + Arrays.toString(arrayToTest));
        reverseTims(arrayToTest);
        System.out.println("Reversed passed : " + Arrays.toString(arrayToTest));
    }

    private static void reverse(int[] array) {

        System.out.println("Original order of array : ");
        for (int i = 0 ; i < array.length ; i++) {
            System.out.print(array[i] + ", " );
        }

        System.out.println("\n\nReversed order of array : ");
        for (int i = array.length-1 ; i >= 0 ; i--) {
            System.out.print(array[i] + ", ");
        }

    }

    //I had to reverse array not to display reversed only
    private static void reverseTims(int[] array) {
        int maxIndex = array.length-1;
        int middlepoint = array.length/2;

        for (int i = 0 ; i < middlepoint ; i++) {
            int temp = array[i];
            array[i] = array[maxIndex -i];
            array[maxIndex-i] = temp;
        }


    }
}
