package com.erlandas;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        //*********WRITE DATA************
        try (FileOutputStream binFile = new FileOutputStream("data.dat");
             FileChannel binChannel = binFile.getChannel()) {
            /*
            Buffers capacity -> its the number of elements buffer can contain

            Buffers position -> is the index of the next element that should be read or
                                written it cant be greater the buffers capacity

            Buffers mark ->     used by the buffer.reset method when this method called
                                buffers position reset to its mark

                   When you want to rewind buffer to certain point you mark the point
                   then later call the reset method to reset the position to the mark
             */

            ByteBuffer buffer = ByteBuffer.allocate(100);   //--> we allocated more buffer then we need

            //                             --getBytes() called to create byte array
            byte[] outputBytes = "Hello World!".getBytes();

            buffer.put(outputBytes);
            buffer.putInt(245);
            buffer.putInt(-98765);

            byte[] outputBytes2 = "Nice to meet you".getBytes();
            buffer.put(outputBytes2);
            buffer.putInt(1000);
            buffer.flip();  //--> we need to call flip() once after we put all the bytes in the buffer

            binChannel.write(buffer);


//            //-- to create byte buffer from the byte array
//            //
//            //-- wrap method does the following
//            //-1-    It wraps the byte array to the buffer
//            //-2-    sets the position of the buffer to 0
//            //-3-    buffers capacity will be set to byte arrays length
//            //-4-    buffers mark will be undefined
//            //ByteBuffer buffer = ByteBuffer.wrap(outputBytes);
//
//            ByteBuffer buffer = ByteBuffer.allocate(outputBytes.length); //--> 3 lines does the same job  as previous line in a code
//            buffer.put(outputBytes);
//            buffer.flip();
//
//            int numBytes = binChannel.write(buffer); //this method returns Int
//
//            System.out.println("numBytes written was: " + numBytes);
//
//            //-- we calling ByteBuffer.allocate() method we passing the size we want buffer to be
//            //   in this case its the number of bytes in Integer
//            ByteBuffer intBuffer = ByteBuffer.allocate(Integer.BYTES);
//
//            intBuffer.putInt(245);
//            intBuffer.flip(); //-- to reset the position to 0
//            numBytes = binChannel.write(intBuffer);
//            System.out.println("numBytes written was: " + numBytes);
//
//            intBuffer.flip(); //-- to reset the position to 0
//            intBuffer.putInt(-98765);
//            intBuffer.flip(); //-- to reset the position to 0
//            numBytes = binChannel.write(intBuffer);
//            System.out.println("numBytes written was: " + numBytes);
//
//
//            /*
//            We have to call buffer.flip() method always when we transferring from writing to reading
//            and vise versa
//             */
//
//
//            //******READING FROM FILE***********
//
//            //1. Using java.io first
//
////            //--    create random access file "rwd" read write permissions
////            RandomAccessFile ra = new RandomAccessFile("data.dat","rwd");
////
////            //--    define new byte array it will contain the data we reading
////            byte[] b = new byte[outputBytes.length];
////
////            //--    what we reading its going to the byte array
////            ra.read(b);
////            System.out.println(new String(b));
////
////            //**Next read two integers we entered
////
////            long intOne = ra.readInt();
////            long intTwo = ra.readInt();
////            System.out.println(intOne);
////            System.out.println(intTwo);
//
//            //2. Using java.NIO
//
//            //--    create random access file "rwd" read write permissions
//            RandomAccessFile ra = new RandomAccessFile("data.dat","rwd");
//
//            //--    define the channel
//            FileChannel channel = ra.getChannel();
//
//            outputBytes[0] = '1';
//            outputBytes[1] = '2';
//            buffer.flip(); //-- flip() important DON'T forget when changing from write to read / read to write
//            //--    to get size of buffer how many bytes to read
//            long numBytesRead = channel.read(buffer);
////            System.out.println("outputBytes = " + new String(outputBytes));
////            System.out.println(numBytesRead);
//
//            if(buffer.hasArray()) {
//                System.out.println("byte buffer = " + new String(buffer.array()));
////                System.out.println("byte buffer = " + new String(outputBytes));
//            }
//
//            //ABSOLUTE READ
//            intBuffer.flip();
//            numBytesRead = channel.read(intBuffer);
//            System.out.println(intBuffer.getInt(0));//-- 0 -> going to read at the index position we specified
//            intBuffer.flip();
//            numBytesRead = channel.read(intBuffer);
//            intBuffer.flip();
//            System.out.println(intBuffer.getInt(0));//-- 0 -> going to read at the index position we specified //Absolute read
//            System.out.println(intBuffer.getInt());        //--> relative read
//
//
//            //**to read following integers added to .dat file
//            // RELATIVE READ
//            //            intBuffer.flip(); //--   reset buffer
//            //            numBytesRead = channel.read(intBuffer);
//            //            intBuffer.flip();
//            //            System.out.println(intBuffer.getInt());
//            //            intBuffer.flip();
//            //            numBytesRead = channel.read(intBuffer);
//            //            intBuffer.flip();
//            //            System.out.println(intBuffer.getInt());
//
//            //Close everything after completed
//            channel.close();
//            ra.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
