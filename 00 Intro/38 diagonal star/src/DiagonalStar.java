public class DiagonalStar {

    public static void printSquareStar(int number) {

        if (number < 5) System.out.println("Invalid Value");
        else {
            for (int col = 1; col <= number; col++){
                for (int row = 1; row <=number; row++){
                    if (row == 1 || col == 1 || col == number || row == number){
                        System.out.print("*");
                    }
                    else if (row == col){
                        System.out.print("*");
                    }
                    else if (col == number-row+1) {
                        System.out.print("*");
                    }
                    else System.out.print(" ");
                }
                System.out.println();
            }
        }
    }
}
