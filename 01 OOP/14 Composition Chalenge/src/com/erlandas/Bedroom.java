package com.erlandas;

public class Bedroom {
    private String bed;
    private String curtains;
    private String shower;
    private Dimensions windowDimensions;
    private Dimensions doorDimensions;

    public Bedroom(String bed, String curtains, String shower, Dimensions windowDimensions, Dimensions doorDimensions) {
        this.bed = bed;
        this.curtains = curtains;
        this.shower = shower;
        this.windowDimensions = windowDimensions;
        this.doorDimensions = doorDimensions;
    }

    public void closeCurtains() {
        System.out.println("Bedrooms curtains closed");
    }

    public void lockDorrs() {
        System.out.println("Bedrooms doors are locked");
    }

    public void sexyTime() {
        System.out.println("Having sexy time in a bedroom\n............");
        useShower();

    }

    public void useShower() {
        System.out.println(this.shower+ " shower is being used");
    }

    public String getBed() {
        return bed;
    }

    public String getCurtains() {
        return curtains;
    }

    public String getShower() {
        return shower;
    }

    public Dimensions getWindowDimensions() {
        return windowDimensions;
    }

    public Dimensions getDoorDimensions() {
        return doorDimensions;
    }
}
