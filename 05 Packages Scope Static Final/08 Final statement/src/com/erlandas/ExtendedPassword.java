package com.erlandas;

public class ExtendedPassword extends Password {
    private int decryptedPassword;

    public ExtendedPassword(int password) {
        super(password);
        this.decryptedPassword = password;
    }

    /*
    in Password class added word final in storePassword method to
    restrict access to a password
     */

    //@Override
//    public void storePassword() {
//        System.out.println("Saving password as : " +this.decryptedPassword);
//    }
}
