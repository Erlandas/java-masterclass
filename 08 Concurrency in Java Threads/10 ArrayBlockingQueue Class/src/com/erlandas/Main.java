package com.erlandas;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

    public static final String EOF = "EOF";

    public static void main(String[] args) {
        /*
        Problem happens when all the threads wants to access same list at the same time
        thats why we synchronizing in our code list in the other classes
        to allow threads to complete their tasks before passing to another thread
         */

        //Array blocking Queue
        ArrayBlockingQueue<String> buffer = new ArrayBlockingQueue<String>(6);

        //--    Result of arrayBlockingQueue we no longer need bufferLock
        //ReentrantLock bufferLock = new ReentrantLock();

        //--    Initialize executors with amount of threads to execute

        /*
        For this kind of program tu use executive services is OVERKILL
        best used when dealing with large number of threads because executing services will optimize program
         */
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        MyProducer producer = new MyProducer(buffer,ThreadColor.ANSI_YELLOW);
        MyConsumer consumer1 = new MyConsumer(buffer, ThreadColor.ANSI_PURPLE);
        MyConsumer consumer2 = new MyConsumer(buffer, ThreadColor.ANSI_CYAN);

        //execute threads instead of using start()
        executorService.execute(producer);
        executorService.execute(consumer1);
        executorService.execute(consumer2);

        Future<String> future = executorService.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println(ThreadColor.ANSI_WHITE + "I'm being printed from the Callable class");
                return "This is the callable result";
            }
        });

        //Callable has to wait until all the threads initialized (3) are finished
        try {
            System.out.println(future.get());
        } catch (ExecutionException e) {
            System.out.println("Something went wrong");
        } catch (InterruptedException e) {
            System.out.println("Thread running the task was interrupted");
        }

        //--    When shutdown called executive services will wait to finish remaining task to execute
        //--    And then shutdown
        //--    If it wont be shutdown() threads will finish but program wont exit
        executorService.shutdown();

        /*
        new Thread(producer).start();
        new Thread(consumer1).start();
        new Thread(consumer2).start();
         */





    }
}
