package com.erlandas;

import java.util.ArrayList;

public class Bank {
    private String bankName;
    private ArrayList<Branch> branches;

    public Bank(String bankName) {
        this.bankName = bankName;
        this.branches = new ArrayList<Branch>();
    }

    public boolean addBranch(String branchName) {
        if(findBranch(branchName) == null) {
            this.branches.add(new Branch(branchName));
            return true;
        }

        return false;
    }

    public boolean addCustomerToBranch(String branchName,String customerName,double amount) {
        Branch branch = findBranch(branchName);
        if(branch != null) {
            return branch.newCustomer(customerName,amount);
        }

        return false;
    }

    public boolean addCustomerTransaction(String branchName,String customerName,double amount) {
        Branch branch = findBranch(branchName);
        if(branch != null) {
            return branch.addTransactions(customerName,amount);
        }

        return false;
    }

    private Branch findBranch(String branchName) {
        for (int i = 0 ; i < branches.size() ; i++) {
            Branch currentBranch = this.branches.get(i);
            if(currentBranch.getBranchName().equals(branchName)) {
                return currentBranch;
            }
        }

        return null;
    }


    public boolean listCustomers(String branchName, boolean showTransactions) {
        Branch branch = findBranch(branchName);
        if(branch != null) {
            System.out.println("Customer details for branch " + branch.getBranchName());
            ArrayList<Customer> listCustomers = branch.getCustomers();

            for (int i = 0 ; i < listCustomers.size() ; i++) {
                Customer cCustomer = listCustomers.get(i);
                System.out.println("[" + i + "] Customer: " + cCustomer.getCustomerName());

                if(showTransactions) {
                    ArrayList<Double> transactions = cCustomer.getTransactions();
                    //System.out.println("Transactions " + transactions.toString());
                    System.out.println("Transactions : ");
                    for (int j = 0 ; j < transactions.size() ; j++) {
                        System.out.println("[" +(j+1)+ "] amount " + transactions.get(j));
                    }
                }
            }
            return true;
        } else {
            return false;
        }


    }
}
