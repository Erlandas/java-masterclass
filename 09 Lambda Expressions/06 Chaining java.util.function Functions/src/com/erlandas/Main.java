package com.erlandas;
/**
 * Function Interfaces
 *
 * Interface        Functional          Number of       Returns a Value     Can be chained
 *                  Method              Arguments
 * ----------------------------------------------------------------------------------------
 * Consumer         accepts()           1 or 2 (Bi)     No                  Yes
 * Supplier         get()               0               Yes                 No
 * Predicate        test()              1 or 2 (Bi)     Yes - boolean       Yes
 * Function         apply()             1 or 2 (Bi)     Yes                 Yes
 * UnaryOperator    depends on type     1               Yes - same type     Yes
 *                                                          as argument
 * ----------------------------------------------------------------------------------------
 */

/*
java.util.function package :
    That package contains functional interfaces that are meant to be used with lambda expressions.
    Interfaces aren't meant to do anything specific they represent the structure of lambda expressions
    that are commonly used in java.

Consumer :
    It's called consumer because --> object int nothing out.

Predicate :
    Condition passed through method

Function interface :
    Interface represents a function takes one parameter abd returns a value, the functional method the one
    used with lambdas is apply method.

    By using function we can pass code that accepts and returns value to a method in a form of a lambda expression
    and then run that code without having to create the interface and the class that implements the interface,
    we can change what method does based on the function we passed in.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.*;

public class Main {

    public static void main(String[] args) {
        Employee john = new Employee("John Doe", 30);
        Employee erlandas = new Employee("Erlandas Bacauskas", 31);
        Employee jack = new Employee("Jack Hill", 40);
        Employee snow = new Employee("Snow White",22);
        Employee red = new Employee("Red RidingHood", 35);
        Employee charming = new Employee("Prince Charming", 31);

        List<Employee> employees = new ArrayList<>();
        employees.add(john);
        employees.add(erlandas);
        employees.add(jack);
        employees.add(snow);
        employees.add(red);
        employees.add(charming);

        System.out.println("\n=================================");
        //Get last name Function :
        //      We used generics with function
        //      The first type is the argument type in this case it's Employee
        //      Second type its the RETURN type in this case it's String
        Function<Employee, String> getLastName = (Employee employee) -> {
            return employee.getName().substring(employee.getName().indexOf(' ') + 1);
        };

        //Get last name of the second employee in the list
        String lastName = getLastName.apply(employees.get(1));
        System.out.println(lastName);

        //Get first name Function :
        Function<Employee, String> getFirstName = (Employee employee) -> {
            return employee.getName().substring(0,employee.getName().indexOf(' '));
        };

        //Use of the function getFirstName
        String firstName = getFirstName.apply(employees.get(1));
        System.out.println(firstName);

        //Now we apply created method getAName

        System.out.println("\n=================================");
        Random random1 = new Random();
        for (Employee employee : employees) {
            if(random1.nextBoolean()) {
                System.out.println("Name : " + getAName(getFirstName,employee));
            } else {
                System.out.println("Surname : " + getAName(getLastName,employee));
            }
        }

        //--    Function that upper cases the name
        Function<Employee,String> upperCase = employee -> employee.getName().toUpperCase();
        //--    Function that returns first name
        Function<String,String> theFirstName = name -> name.substring(0,name.indexOf(' '));
        //--    Now chain both functions together and print out the result
            //  1.  First upper cases passes object, sends name as argument for second function
            //  2.  Then returns substring , name
        Function chainedFunction = upperCase.andThen(theFirstName);
        System.out.println(chainedFunction.apply(employees.get(0)));

        //--    BiFunction accepts TWO arguments <Arg1, Arg2, Return Type>
        BiFunction<String, Employee, String> concatAge = (String name, Employee employee) -> {
            return name.concat(" " + employee.getAge());
        };

        System.out.println("\n=================================");
        String upperName = upperCase.apply(employees.get(1));
        System.out.println(concatAge.apply(upperName,employees.get(1)));

        //--    Create IntUnaryOperator that always increments its argument by given integer
        System.out.println("\n=======IntUnaryOperator=========");
        IntUnaryOperator incByNumber = i -> i + 5 ;
        System.out.println(incByNumber.applyAsInt(10));

        //--    Chained CONSUMER example
        System.out.println("\n=======Chained Consumer=========");
        Consumer<String> c1 = s -> s.toUpperCase();
        Consumer<String> c2 = s -> System.out.println(s);
            //remember that consumers don't return anything so result
            //to upper case actually lost
            //In this example it's no advantage to chain them together
            //But it could be chained together if needed
        c1.andThen(c2).accept("Hello, World!");

    }

    //--    Method that accepts a function employee string argument
    private static String getAName (Function<Employee,String> getName, Employee employee) {
        return getName.apply(employee);
    }

    //--    Method to print employees by age
    private static void printEmployeesByAge(List<Employee> employees,
                                            String ageText,
                                            //for loop passes each employee to the predicates test method
                                            //which in turn uses the employee as a parameter for the lambda expression
                                            //that maps to the predicate
                                            Predicate<Employee> ageCondition) {
        System.out.println(ageText);
        System.out.println("====================");

        for (Employee employee : employees) {
            if (ageCondition.test(employee)) {  //--    Test method tests condition with passed employee object
                System.out.println(employee.getName());
            }

        }
    }

}
