package com.erlandas;

public class BankAccount {

    private int accNumber;
    private double balance;
    private String customerName;
    private String email;
    private String phoneNumber;

    //Constructors
    public BankAccount(){
        //To call overloaded constructor passing default parameters if parameters wasnt supplied
        //This within empty constructor calls a constructor !!!THIS!!!
        //It has to be the first thing in an empty constructo otherwise it will be error
        this(0,0,"Default name","Default email","Default number");
    }

    public BankAccount(int accNumber, double balance, String customerName, String email, String phoneNumber) {
        this.accNumber = accNumber;
        this.balance = balance;
        this.customerName = customerName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public BankAccount(String customerName, String email, String phoneNumber) {
        this((int)(Math.random()*Math.pow(10,9)),0.0,customerName,email,phoneNumber);
        //This way arguments passed are sent to main constructor by defaulting first two values
    }

    //SETTERS
    public void setAccNumber(int accNumber) {
        this.accNumber = accNumber;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    //GETTERS
    public int getAccNumber() {
        return this.accNumber;
    }

    public double getBalance() {
        return this.balance;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public String getEmail() {
        return this.email;
    }

    //Methods
    public void toDeposit(double amount) {
        this.balance += amount;
        System.out.println("Before deposit : "+(this.balance-amount)+"\nAfter deposit : "+this.balance);
    }

    public void toWithdraw(double amount) {
        if((this.balance - amount) < 0) {
            System.out.println("Insuficient funds your balance is : " + this.balance);
        } else {
            this.balance -= amount;
            System.out.println("Before withdraw : "+(this.balance+amount)+"\nAfter withdraw : "+this.balance);
        }


    }
}
