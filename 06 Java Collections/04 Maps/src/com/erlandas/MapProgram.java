package com.erlandas;

import java.util.HashMap;
import java.util.Map;

public class MapProgram {

    public static void main(String[] args) {
        Map<String, String> languages = new HashMap<>();


        /*
        In maps key values is unique if we try to add second entry with same name
        values becomes overwritten

        For particular key it can be only one value
         */

        //To check even before adding

        if(languages.containsKey("Java")) {
            System.out.println("Java already exists");
        } else {
            languages.put("Java", "a compiled high level, object-oriented, platform independent language");
        }

        //That's now stored key value pair in to map object

        languages.put("Python", "an interpreted, object-oriented, high-level programming language " +
                "with dynamic semantics");
        languages.put("Algol", "an algorithmic language");

        //In this case if returned NULL that means it was brand new object and it wasn't overwritten
        System.out.println(languages.put("BASIC", "Beginners All Purposes Symbolic Instruction Code"));
        System.out.println(languages.put("Lisp", "Therein lies madness"));

        //System.out.println(languages.get("Java"));

        //Overwriting example
        //If language is overwritten it returns previous value using sout
//        System.out.println(languages.put("Java","This course is about Java"));
//        System.out.println(languages.get("Java"));

        //Example how to add value with checking if value already exists

        if(languages.containsKey("Java")) {
            System.out.println("Java is already in the Map");
        } else {
            languages.put("Java","This course is about Java");
        }

        System.out.println("================================================================");


        //To remove key entry and value

        //languages.remove("Lisp");


        //returns boolean
        if(languages.remove("Algol","an algorithmic language")) {
            System.out.println("Algol removed");
        } else {
            System.out.println("Algol not removed, key/value pair not found");
        }

        System.out.println("================================================================");

        //To replace value of key

        //If language exist for replace returns previous value
//        System.out.println(languages.replace("Lisp","a functional programming language with imperative features"));
        //if not exists returns NULL
//        System.out.println(languages.replace("Scala", "this will not be added"));

        //TO CHECK FOR REPLACING
        //                      (key  , old value   ,   new value)
        if(languages.replace("Lisp","This will not work","a functional programming language with imperative features")) {
            System.out.println("Lisp replaced");
        } else {
            System.out.println("Lisp was not replaced");
        }


        System.out.println("================================================================");
        //To loop through the objects in Map SYNTAX
        for (String key: languages.keySet()) {
            System.out.println(key + " : " + languages.get(key));
        }























    }
}
