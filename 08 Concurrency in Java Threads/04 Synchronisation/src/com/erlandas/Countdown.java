package com.erlandas;

public class Countdown {

    private int i;

    public void doCountdown() {
        String color;

        switch(Thread.currentThread().getName()) {
            case "Thread 1":
                color = ThreadColor.ANSI_CYAN;
                break;
            case "Thread 2":
                color = ThreadColor.ANSI_PURPLE;
                break;
            default:
                color = ThreadColor.ANSI_GREEN;
        }
        /*
        By synchronizing we allowing for one thread to finish work
        before next one starts

        When we synchronizing code we must synchronize only components
        that must be synchronized

        Keep the code that we synchronize to absolute minimum
         */

        synchronized (this) {
            for (i = 10 ; i > 0 ; i--) {
                System.out.println(color + Thread.currentThread().getName() + ": i = " + i);
            }
        }


    }
}
