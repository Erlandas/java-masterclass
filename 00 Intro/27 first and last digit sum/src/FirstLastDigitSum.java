public class FirstLastDigitSum {

    public static int sumFirstAndLastDigit(int number) {

        if (number < 0) return -1;
        else if (number < 10) return number+number;

        int sumFirstAndLast = 0;
        int original = number;

        while(number > 0) {

            if (original == number) {
                sumFirstAndLast += original%10;
                original = 0;
            }
            if(number<10) {
                sumFirstAndLast += number;
            }


            number /= 10;
        }


        return sumFirstAndLast;
    }
}
