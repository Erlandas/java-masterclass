package com.erlandas;

import java.io.IOException;
import java.nio.file.*;

public class Main {

    public static void main(String[] args) {

//        DirectoryStream.Filter<Path> filter =
//                new DirectoryStream.Filter<Path>() {
//                    public boolean accept(Path path) throws IOException {
//                        return (Files.isRegularFile(path));
//                    }
//                };

        //LAMBDA

        DirectoryStream.Filter<Path> filter = p -> Files.isRegularFile(p);

        //--   Creating the path to the directory
        Path directory = FileSystems.getDefault().getPath("Filetree\\Dir2");

        //--    We creating directory stream in resources section
        //--    To get the contents in that folder                              // What type of file to print
        try(DirectoryStream<Path> contents = Files.newDirectoryStream(directory, filter)) {

            //--    Iterate through directories contents
            for (Path file: contents) {
                System.out.println(file.getFileName());
            }

            //--> | <-- inclusive OR use instead of making couple of catch blocks
        } catch (IOException | DirectoryIteratorException e) {
            System.out.println(e.getMessage());
        }

    }

}

//https://docs.oracle.com/javase/8/docs/api/java/nio/file/FileSystem.html#getPathMatcher-java.lang.String-
