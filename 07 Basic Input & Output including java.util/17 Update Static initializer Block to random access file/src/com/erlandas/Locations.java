package com.erlandas;

import java.io.*;
import java.util.*;

public class Locations implements Map<Integer, Location> {

    private static Map<Integer, Location> locations = new LinkedHashMap<Integer, Location>();
    private static Map<Integer, IndexRecord> index = new LinkedHashMap<>();
    private static RandomAccessFile ra;

    public static void main(String[] args) throws IOException {

//
//--  Use an index (memory-based) to access the locations data.  Since the location records are
//--  not fixed length then each index record will have to have the location record key,  the
//--  starting offset to the location record and a length value for the location record.
//
//--  File Structure  (includes index data and game data)  (assuming 4 bytes per integer)
//--      bytes     length  description
//--    0000-0003       4   number of location records in the data file
//--    0004-0007       4   offset to start of data file (game location records)
//--    0008-1699    1692   index data
//--                            structure:   00-03   locationID key
//--                                         04-07   record offset
//--                                         08-12   record length
//--    1700-eof     ????   game data (location records)
//
        try (RandomAccessFile rao = new RandomAccessFile("locations_rand.dat","rwd"))  {
            //  0000-0004  number of location records  (game data)
            rao.writeInt(locations.size());

            //  0004-0007  offset to start of game data
            int indexSize = locations.size()           // # of records in the index
                    * 3                          // # of integers in an index entry
                    * Integer.BYTES;             // # bytes for an integer
            int locationStart = (int) (indexSize       // size of index (calculated above)
                    + rao.getFilePointer()   // current file pointer location
                    + Integer.BYTES);        // size of this number
            rao.writeInt(locationStart);


            long indexStart = rao.getFilePointer();
            int startPointer = locationStart;   //  set the offset for the first location in to a viariable
                                                //  we need this value to calculate locations record length
                                                //  after we written to a file

            rao.seek(startPointer);      // we used seek() to move the file pointer to the first locations offset
                                         // we only have to do this for the first location
                                         // because after that we write all data sequentely

            for (Location location : locations.values()) {
                rao.writeInt(location.getLocationID());
                rao.writeUTF(location.getDescription());
                StringBuilder builder = new StringBuilder();

                for (String direction : location.getExists().keySet()) {
                    if(!direction.equalsIgnoreCase("Q")) {
                        builder.append(direction);
                        builder.append(",");
                        builder.append(location.getExists().get(direction));
                        builder.append(",");
                    }
                }
                rao.writeUTF(builder.toString());

                /*
                create index record for it
                 */
                IndexRecord record = new IndexRecord(startPointer, (int) (rao.getFilePointer() - startPointer));
                index.put(location.getLocationID(), record);

                startPointer = (int) rao.getFilePointer();
            }

            rao.seek(indexStart);
            for (Integer locationID : index.keySet()) {
                rao.writeInt(locationID);
                rao.writeInt(index.get(locationID).getStartByte());
                rao.writeInt(index.get(locationID).getLength());
            }


        }
    }

    //1.    This first four bytes will contain the number of locations (bytes 0 - 3).
    //2.    The next four bytes will contain the start offset of the locations section (bytes 4 - 7).
    //3.    The next section of the file will contain the index (the index is 1692 bytes long).
    //      It will start at byte 8 and end at byte 1699 (bytes 8 - 1699).
    //4.    The final section of the file will contain the location records (the data). it will start at byte 1700.

    static {

        try {
            ra = new RandomAccessFile("locations_rand.dat","rwd");
            int numLocations = ra.readInt();
            long locationStartPoint = ra.readInt();

            while(ra.getFilePointer() < locationStartPoint) {
                int locationID = ra.readInt();
                int locationStart = ra.readInt();
                int locationLength = ra.readInt();

                IndexRecord record = new IndexRecord(locationStart,locationLength);
                index.put(locationID,record);
            }


        } catch (IOException e) {
            System.out.println("IO Exception in static initializer: " + e.getMessage());
        }

        //USING BYTE STREAMS

//        try(ObjectInputStream locFile = new ObjectInputStream(new BufferedInputStream(new FileInputStream("locations.dat")))) {
//
//            boolean eof = false;
//            while (!eof) {
//                try {
//                    Location location = (Location) locFile.readObject();
//                    System.out.println("Read location " + location.getLocationID() + " : " + location.getDescription());
//                    System.out.println("Found " + location.getExists().size() + " exits");
//
//                    locations.put(location.getLocationID(), location);
//                } catch (EOFException io) {
//                    eof = true;
//                }
//            }
//        } catch (InvalidClassException e) {
//            System.out.println("InvalidClassException " + e.getMessage());
//        } catch (IOException e) {
//            System.out.println("IO Exception " + e.getMessage());
//        } catch (ClassNotFoundException e) {
//            System.out.println("ClassNotFoundException " + e.getMessage());
//        }


    }

    @Override
    public int size() {
        return locations.size();
    }

    @Override
    public boolean isEmpty() {
        return locations.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return locations.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return locations.containsValue(value);
    }

    @Override
    public Location get(Object key) {
        return locations.get(key);
    }

    @Override
    public Location put(Integer key, Location value) {
        return locations.put(key, value);
    }

    @Override
    public Location remove(Object key) {
        return locations.remove(key);
    }

    @Override
    public void putAll(Map<? extends Integer, ? extends Location> m) {

    }

    @Override
    public void clear() {
        locations.clear();
    }

    @Override
    public Set<Integer> keySet() {
        return locations.keySet();
    }

    @Override
    public Collection<Location> values() {
        return locations.values();
    }

    @Override
    public Set<Entry<Integer, Location>> entrySet() {
        return locations.entrySet();
    }
}
