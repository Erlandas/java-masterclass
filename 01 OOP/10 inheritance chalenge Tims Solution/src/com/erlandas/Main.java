package com.erlandas;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Outlander outlander = new Outlander(36);

        outlander.steer(45);
        outlander.accelerate(47);
        outlander.accelerate(-10);
    }
}
