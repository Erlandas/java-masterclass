package com.erlandas;

/*
wait() ->   we calling wait within a loop that's testing for whatever condition we waiting
            because when the thread is notified to wake up it's not guarantee that's being woken up
            because condition we waiting on was changed

notifyAll() ->  we cant notify specific thread
                we notify threads to resume executing code

 */

public class Message {

    private String message;
    private boolean empty = true;

    public synchronized String read() {
        while(empty) {
            try {
                wait();
            } catch (InterruptedException e) {

            }
        }
        empty = true;
        notifyAll();
        return message;
    }

    public synchronized void write(String message) {
        while(!empty) {
            try {
                wait();
            } catch (InterruptedException e) {

            }

        }
        empty = false;
        this.message = message;
        notifyAll();
    }
}
