public class GreatestCommonDivisor {

    public static int getGreatestCommonDivisor(int num1, int num2) {

        if (num1 < 10 || num2 < 10) return -1;

        int loopStart;

        if (num1 <= num2) loopStart = num1;
        else loopStart = num1;

        for (int i = loopStart ; i > 1 ; i-- ) {

            if(num1 % i == 0 && num2 % i ==0) {
                return i;
            }
        }

        return 1;

    }
}
