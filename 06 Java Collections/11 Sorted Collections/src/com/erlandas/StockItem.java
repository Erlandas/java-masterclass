package com.erlandas;

public class StockItem implements Comparable<StockItem> {

    private final String name;
    private double price;
    private int quantityStock; //Can be initialized later

    //Constructors
    public StockItem(String name, double price) {
        this.name = name;
        this.price = price;
        this.quantityStock = 0; //Or here (but you wouldn't normally do both)
    }

    public StockItem(String name, double price, int quantityStock) {
        this.name = name;
        this.price = price;
        this.quantityStock = quantityStock;
    }

    //Getters
    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int quantityInStock() {
        return quantityStock;
    }

    //Setters

    public void setPrice(double price) {
        //Check if price is greater the zero
        if(price > 0.0) {
            this.price = price;
        }
    }

    //Methods
    public void adjustStock(int quantity) {
        int newQuantity = this.quantityStock + quantity;
        if(newQuantity >= 0) {
            this.quantityStock = newQuantity;
        }
    }

    //Override equals()
    @Override
    public boolean equals(Object obj) {
        System.out.println("Entering StockItem.equals()");
        //If object is the same in memory checking if comparing same objects so its true
        if(obj == this) {
            return true;
        }

        //If object is null/empty or class is different return false
        if(obj == null || (obj.getClass() != this.getClass())) {
            return false;
        }

        String objName = ((StockItem) obj).getName();
        //compares equality by object name returns true if name is exactly the same and false if its not
        return this.name.equals(objName);
    }

    //Override hashCode()
    @Override
    public int hashCode() {
        return this.name.hashCode() + 31;
    }

    //Override compareTo()
    @Override
    public int compareTo(StockItem o) {
        System.out.println("Entering StockItem.compareTo()");
        //ths --> its the same object in memory equals to passed object o
        if(this == o) {
            return 0;
        }

        if(o != null) {
            //Sends object to inbuilt String compareTo method
            return this.name.compareTo(o.getName());
        }

        throw new NullPointerException();
    }

    //Override toString()
    @Override
    public String toString() {
        return this.name + " : price " + this.price;
    }

































}
