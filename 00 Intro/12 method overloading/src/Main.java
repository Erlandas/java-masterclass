public class Main {

    public static void main(String[] args) {

        int newScore = calculateScore("Erlandas", 500);
        System.out.println("New Score: " + newScore);

        calculateScore(75);

        calculateScore();


        //Chalenge starts

        calcFeetAndInchesToCentimeters(100);


    }

    //CHALENGE START

    public static double calcFeetAndInchesToCentimeters(double feet, double inches) {
        if(feet >=  0 && (inches >= 0 && inches <= 12)) {

            System.out.println(feet + " feet " + inches + " inches is equal to "  + ((inches + (feet * 12)) * 2.54) + " cm");
            return (inches + (feet * 12)) * 2.54;
        }
        System.out.println("-1");
         return -1;
    }

    public static double calcFeetAndInchesToCentimeters(double inches) {
        if(inches >= 0 ) {

            System.out.println(inches + " inches is equal to " + ((int)(inches/12)) + " feet " + inches%12 + " inches");
            return calcFeetAndInchesToCentimeters(((int)(inches/12)),inches%12);
        }

        System.out.println("-1");
        return -1;
    }

    //CHALENGE END

    /* Overloading method is when we create identical methods with different number of arguments accepted
    *  Changing return data type does not make method unique we have to change number of arguments or types
    *  of arguments*/

    public static int calculateScore(String playerName, int score) {

        System.out.println("Player " + playerName + " scored " + score + " points");
        return score * 1000;
    }

    public static int calculateScore(int score) {

        System.out.println("Unnamed player scored " + score + " points");
        return score * 1000;
    }

    public static int calculateScore() {

        System.out.println("No player name, no player score");
        return 0;
    }



}
