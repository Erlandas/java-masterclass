package com.erlandas;

public class Main {

    public static void main(String[] args) {

        Account erlandasAcc = new Account("Erlandas");

        erlandasAcc.deposit(1000);
        erlandasAcc.withdraw(500);
        erlandasAcc.withdraw(-200);
        erlandasAcc.deposit(-20);

        erlandasAcc.calculateBalance();

        System.out.println("Erlandas balance is " + erlandasAcc.getBalance());
    }
}
