public class Main {

    public static void main(String[] args) {

        int theDigit = 123456789;
        System.out.println("The sum of the digits " + theDigit + " is " + sumDigits(theDigit));

    }

    public static int sumDigits(int number) {
        if(number < 10) {
            return -1;
        }

        int sum = 0;

        while(number > 0) {
            sum += number % 10;
            number /= 10;
        }

        return sum;
    }
}
