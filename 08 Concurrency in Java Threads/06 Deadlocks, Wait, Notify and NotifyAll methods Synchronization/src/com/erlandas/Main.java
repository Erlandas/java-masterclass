package com.erlandas;

public class Main {
    /*
    In this example we will be pretending
    that we reading and writing messages of some sort

    We will have two threads in our application
    Thread 1 -> will be producing messages
    Thread 2 -> consume messages
     */

    public static void main(String[] args) {

        Message message = new Message();
        (new Thread(new Writer(message))).start();
        (new Thread(new Reader(message))).start();

    }
}
