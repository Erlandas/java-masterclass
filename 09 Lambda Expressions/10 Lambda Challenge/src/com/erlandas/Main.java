package com.erlandas;

/**
 * Challenge #8 :
 *    --  Question :
 *      there are many interfaces in the Java SDK, and sometimes we can use a lambda
 *      expression instead of creating an instance that implements the interface we
 *      want to use.
 *
 *      Given a specific interface, how can we tell whether we can map a lambda
 *      expression to it? What's the criteria that has to be met?
 *
 *    --  Answer :
 *      The interface has to be a functional interface. It can have only a
 *      single method that must be implemented.
 *
 *      A functional interface can contain more than one method, but all the methods
 *      but one must have default implementations.
 *
 *      Most of the time, the documentation for an interface will state whether it's a
 *      functional interface.
 *
 *    --  Question :
 *      With that in mind, can we use a lambda expression to represent an
 *      instance of the java.util.concurrent.Callable interface?
 *
 *      HINT: You'll have to check the documentation. As a Java developer, you have
 *      to be comfortable with looking up and reading documentation.
 *
 *    --  Answer :
 *      The Callable interface has only one method that has to be
 *      implemented - the call() method.
 *
 *      So we can use a lambda for it. The documentation also states that it's a
 *      functional interface.
 *
 *    --  Question :
 *      Is the java.util.Comparator interface a functional interface?
 *
 *    --  Answer :
 *      Yes, it is. Despite containing over 10 methods, only one method has
 *      to be implemented - compare().
 *      Because of that, it's a functional interface.
 *
 */


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.Comparator;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
    /**
     * Challenge #1 :
     *      Write the following anonymous class as a lambda expression
     *
     *     Runnable runnable = new Runnable() {
     *         @Override
     *         public void run() {
     *             String myString = "Let's split this up into an array";
     *             String[] parts = myString.split(" ");
     *             for (String part : parts) {
     *                 System.out.println(part);
     *             }
     *         }
     *     };
     */
        Runnable runnable = () -> {
            String myString = "Let's split this up into an array";
            String[] parts = myString.split(" ");
                for (String part : parts) {
                    System.out.println(part);
                }
        };
    //-----------------------------------------------------------------------------------------------
        /**
         * Challenge #2 :
         *      Write the following method as a lambda expression. Dont worry about
         *      assigning it to anything
         *
         *      public static String everySecondChar(String source) {
         *         StringBuilder returnVal = new StringBuilder();
         *         for (int i = 0 ; i < source.length() ; i++) {
         *             if(i % 2 == 1) {
         *                 returnVal.append(source.charAt(i));
         *             }
         *         }
         *         return returnVal.toString();
         *     }
         *
         */

        Function<String,String> lambdaFunction = source -> {
            StringBuilder returnVal = new StringBuilder();
            for (int i = 0 ; i < source.length() ; i++) {
                if(i % 2 == 1) {
                    returnVal.append(source.charAt(i));
                }
            }
            return returnVal.toString();
        };

        //-----------------------------------------------------------------------------------------------
        /**
         * Challenge #3 :
         *      Right now, the function doesn't do anything. Write the code that
         *      will execute the function with an argument of "1234567890"
         */

        System.out.println("Challenge #3 : " +lambdaFunction.apply("1234567890"));
        //-----------------------------------------------------------------------------------------------

        /**
         * Challenge #5 :
         *      Using the bonus version, call the method with the lamdaFunction we created
         *      earlier and the string "1234567890". Print the result returned from the method.
         */
        System.out.println("Challenge #5 : " + everySecondCharacter(lambdaFunction,"1234567890"));

        //-----------------------------------------------------------------------------------------------
        /**
         * Challenge #6 :
         *      Now write a lambda expression that maps to the java.util.Supplier interface. this
         *      lambda should return the string "I love Java!". Assign it to a variable called
         *      iLoveJava
         */
        Supplier<String> iLoveJava = () -> "I love Java!";

        //-----------------------------------------------------------------------------------------------
        /**
         * Challenge #7 :
         *      As with Function, the supplier won't do anything until we use it. Use this supplier
         *      to assign the string "I love Java!" to a variable called supplierResult. Then print
         *      the variable to the console.
         */
        String supplierResult = iLoveJava.get();
        System.out.println(supplierResult);

        //-----------------------------------------------------------------------------------------------
        /**
         * Challenge # 9 :
         *      Write code to print the items in the list in sorted order, and with the first letter in each
         *      name upper-cased. Use lambda expressions wherever is possible.
         */

        List<String> topnames2015 = Arrays.asList(
                "Amelia",
                "Olivia",
                "emily",
                "Isla",
                "Ava",
                "oliver",
                "Jack",
                "Charlie",
                "harry",
                "Jacob"
        );

//        List<String> firstUpperCaseList = new ArrayList<>();
//        topnames2015.forEach(name ->
//                firstUpperCaseList.add(name.substring(0,1).toUpperCase() + name.substring(1)));
//
//        firstUpperCaseList.sort(String::compareTo);
//        firstUpperCaseList.forEach(s -> System.out.println(s));

        /**
         * Challenge 10 :
         *      Change the code so that it uses method references. Remember that a method
         *      reference looks like Class::MethodName
         */
//        List<String> firstUpperCaseList = new ArrayList<>();
//        topnames2015.forEach(name ->
//                firstUpperCaseList.add(name.substring(0,1).toUpperCase() + name.substring(1)));
//
//        firstUpperCaseList.sort(String::compareTo);
//        firstUpperCaseList.forEach(System.out::println);

        /**
         * Challenge 11 :
         *      Now do the same thing using a stream and chain of stream operations.
         */

        topnames2015
                .stream()
                .map(name -> name.substring(0,1).toUpperCase() + name.substring(1))
                .sorted(String::compareTo)
                .forEach(System.out::println);


        /**
         * Challenge 12 :
         *      Instead of printing out the sorted names, print out how many names being with
         *      the letter 'A' instead
         *
         *      Two Hints :
         *      1.  You'll have to modify the stream chain
         *      2.  You'll have to add another statement to print the number of items
         */

        long namesBeginingWithA = topnames2015
                .stream()
                .filter(s -> s.startsWith("A"))
               // .peek(System.out::println)
                .count();

        System.out.println(namesBeginingWithA);
    }

    /**
     * Challenge #4 :
     *      Instead of executing this function directly, suppose we want to pass it to a
     *      method. Write a method called everySecondCharacter that accepts the function
     *      as a parameter and executes it with the argument "1234567890".
     *
     *      It should return the result of the function. For bonus points, don't hard-code the
     *      argument string with the method.
     */

    public static String everySecondCharacter(Function<String,String> func,String source) {
        return func.apply(source);
    }





























}
