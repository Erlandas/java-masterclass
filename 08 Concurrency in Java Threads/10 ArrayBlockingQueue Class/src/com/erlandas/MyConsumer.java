package com.erlandas;


import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

import static com.erlandas.Main.EOF;

public class MyConsumer implements Runnable {

    private ArrayBlockingQueue<String> buffer;
    private String color;

    public MyConsumer(ArrayBlockingQueue<String> buffer, String color) {
        this.buffer = buffer;
        this.color = color;
    }

    //-- every single block of code in the IF statements after its continue or break
    //-- if we wont have there unlock statement it will crash because it wont reach end statement
    //-- with unlock statement

    public void run() {
        while(true) {
            /*
            Only if tryLock returns true it will execute -> that means lock is available
            At that point if lock was available we get that lock automatically and it will go
            And execute that code in the try
             */
            synchronized (buffer) {
                try {
                    if(buffer.isEmpty()) {
                        continue;
                    }
                    if(buffer.peek().equals(EOF)) { //peek --> looks at item
                        System.out.println(color + "Exiting");
                        break;
                    } else {
                        System.out.println(color + "Removed" + buffer.take());// take --> removes item
                    }
                } catch (InterruptedException e) {
                }
            }

        }


    }
}
