package com.erlandas;


import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import static com.erlandas.Main.EOF;

public class MyConsumer implements Runnable {

    private List<String> buffer;
    private String color;
    private ReentrantLock bufferLock;

    public MyConsumer(List<String> buffer, String color, ReentrantLock bufferLock) {
        this.buffer = buffer;
        this.color = color;
        this.bufferLock = bufferLock;
    }

    //-- every single block of code in the IF statements after its continue or break
    //-- if we wont have there unlock statement it will crash because it wont reach end statement
    //-- with unlock statement

    public void run() {
        int counter = 0;
        while(true) {
            /*
            Only if tryLock returns true it will execute -> that means lock is available
            At that point if lock was available we get that lock automatically and it will go
            And execute that code in the try
             */
            if(bufferLock.tryLock()) {
                try {
                    if(buffer.isEmpty()) {
                        continue;
                    }
                    System.out.println(color + "The counter = " + counter);
                    counter = 0;
                    if(buffer.get(0).equals(EOF)) {
                        System.out.println(color + "Exiting");
                        break;
                    } else {
                        System.out.println(color + "Removed" + buffer.remove(0));
                    }
                } finally {
                    bufferLock.unlock();
                }
            } else {
                counter++;
            }

        }
    }
}
