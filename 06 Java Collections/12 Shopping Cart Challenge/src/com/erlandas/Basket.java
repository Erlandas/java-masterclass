package com.erlandas;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class Basket {

    private final String name;
    private final Map<StockItem, Integer> list;

    /*
    TREE MAP EXAMPLE
    compares each item and puts in sorted order
    spends a lot of time for comparing not efficient
     */
    public Basket(String name) {
        this.name = name;
        this.list = new TreeMap<>();
    }

    public int addToBasket(StockItem item, int quantity) {
        if((item != null) && (quantity > 0)) {
            //We checking if this specific item is in basket already
            //We defaulting to zero if it wasn't any item
            int inBasket = list.getOrDefault(item, 0);
            list.put(item, inBasket + quantity);
            return inBasket;
        }

        return 0;
    }

    //++++++++++++++++++++
    public int removeFromBasket(StockItem item, int quantity) {
        if((item != null) && (quantity > 0)) {
            //Check if we have item in basket
            int inBasket = list.getOrDefault(item,0);

            int newQuantity = inBasket - quantity;

            if(newQuantity > 0) {
                list.put(item, newQuantity);
            } else if (newQuantity == 0) {
                list.remove(item);
                return quantity;
            }
        }

        return 0;
    }

    //+++++++++++++++++++++++++++++
    public void clearBasket() {
        this.list.clear();
    }

    public Map<StockItem, Integer> items() {
        return Collections.unmodifiableMap(list);
    }

    @Override
    public String toString() {
        String s = "\nShopping basket " + name + " contains " + list.size() + (list.size()==1? " item\n" : " items\n");
        double totalCost = 0.0;

        for (Map.Entry<StockItem, Integer> item : list.entrySet()) {
            s = s + item.getKey() + ". " + item.getValue() + " purchased\n";
            totalCost += item.getKey().getPrice() * item.getValue();
        }

        return s + "Total cost " + totalCost;
    }
}
