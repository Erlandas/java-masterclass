package com.erlandas;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BankAccount {

    private double balance;
    private String accountNumber;
    private Lock lock;

    public BankAccount(String accountNumber, double initialBalance) {
        this.accountNumber = accountNumber;
        this.balance = initialBalance;
        this.lock  = new ReentrantLock();
        System.out.println("Bank account created " + this.accountNumber +
                " with initial balance of " + this.balance);
    }

    public void deposit(double amount) {
        boolean status = false;

        try {
            if((lock.tryLock(1000, TimeUnit.MILLISECONDS)) ) {
                try {
                    balance += amount;
                    status = true;
                    System.out.println("Deposit of " + amount + " made, current balance " + this.balance);
                } finally {
                    lock.unlock();
                }
            } else {
                System.out.println("Could not get the lock");
            }
        } catch (InterruptedException e) {

        }

        System.out.println("Transaction status = " + status);

    }

    public void withdraw(double amount) {
        boolean status = false;

        try {
            if((lock.tryLock(1000, TimeUnit.MILLISECONDS)) ) {
                try {
                    balance -= amount;
                    status = true;
                    System.out.println("Withdraw of " + amount + " made, current balance " + this.balance);
                } finally {
                    lock.unlock();
                }
            } else {
                System.out.println("Could not get the lock");
            }
        } catch (InterruptedException e) {

        }

        System.out.println("Transaction status = " + status);

    }

//    public void withdraw(double amount) {
//        lock.lock();
//        try {
//            balance -= amount;
//            System.out.println("Withdraw of " + amount + " made, current balance " + this.balance);
//        } finally {
//            lock.unlock();
//        }
//    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void printAccountNumber() {
        System.out.println("Account number = " + accountNumber);
    }
}
