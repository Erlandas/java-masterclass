package com.erlandas;

public class Main {

    public static void main(String[] args) {

        boolean gameover = true;
        int score = 800;
        int levelCompleted = 5;
        int bonus = 100;

        calculateScore(gameover,score,levelCompleted,bonus);

        score = 100;
        levelCompleted = 20;
        bonus = 100;

        calculateScore(gameover,score,levelCompleted,bonus);


    }

    public static void calculateScore (boolean over, int score, int level, int bonus) {
        if(over) {
            int finalScore = score + (level * bonus);
            System.out.println("Your final score is : " + finalScore);
        }
    }
}
