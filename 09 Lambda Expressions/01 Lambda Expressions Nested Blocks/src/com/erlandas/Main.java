package com.erlandas;
/*
Lambdas --> provides easy way to work with interfaces that only have one method
            most often lambda used in anonymous classes

Lambda Expressions has Three Parts :
    example: new Thread(()-> System.out.println("Printing from runnable LAMBDA")).start();
    1.  Argument list   :
    2.  Error token     :   ->
    3.  The body        :   System.out.println("Printing from runnable LAMBDA")
 */


import java.util.*;

public class Main {

    public static void main(String[] args) {

        //--    Example working with objects and other classes
        Employee john = new Employee("John Doe", 30);
        Employee erlandas = new Employee("Erlandas Bacauskas", 31);
        Employee jack = new Employee("Jack Hill", 40);
        Employee snow = new Employee("Snow White",22);

        List<Employee> employees = new ArrayList<>();
        employees.add(john);
        employees.add(erlandas);
        employees.add(jack);
        employees.add(snow);

        AnotherClass anotherClass = new AnotherClass();
        String s = anotherClass.doSomething();
        System.out.println(s);

//        //***    1. Sort employees by name in ascending order without using LAMBDAS
////        Collections.sort(employees, new Comparator<Employee>() {
////            @Override
////            public int compare(Employee employee1, Employee employee2) {
////                return employee1.getName().compareTo(employee2.getName());
////            }
////        });
//
//        //***   2. Sort using LAMBDAS
//        /*
//                    1.  Argument list   :   (Employee employee1,Employee employee2)
//                    2.  Error token     :   ->
//                    3.  The body        :   employee1.getName().compareTo(employee2.getName()
//         */
//        Collections.sort(employees,(Employee employee1,Employee employee2) -> employee1.getName().compareTo(employee2.getName()));
//        //***   3. Sort using LAMBDAS more simplified. Already knows that we will be using Employee type objects
//        Collections.sort(employees,(employee1,employee2) -> employee1.getName().compareTo(employee2.getName()));
//
//        //***   Check the values for employee
//        for (Employee employee : employees) {
//            System.out.println(employee.getName());
//        }
//
//        //$$$   Concatenate and to upper case names using LAMBDAS
//        //saving lambda expression to a variable
//        UpperConcat uc = (s1,s2) -> {
//            String result = s1.toUpperCase() + ", " + s2.toUpperCase();
//            return result;
//        };
//        String sillyString = doStringStuff(uc,employees.get(0).getName(),employees.get(1).getName());
//        System.out.println(sillyString);
    }

    public final static String doStringStuff(UpperConcat uc, String s1, String s2) {
        return uc.upperAndConcat(s1,s2);
    }
}

class Employee {
    private String name;
    private int age;

    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}


interface UpperConcat {
    public String upperAndConcat(String s1, String s2);
}

class AnotherClass {
    public String doSomething() {
//        //***   Same task using lambda expression
        UpperConcat uc = (s1,s2) -> {
            System.out.println("The lambda expression's class is: " + getClass().getSimpleName());
            String result = s1.toUpperCase()+s2.toUpperCase();
            return result;
        };

//        UpperConcat uc = new UpperConcat() {
//            @Override
//            public String upperAndConcat(String s1, String s2) {
//                System.out.println("i (within anonymous class) = " + i);
//                return s1.toUpperCase()+s2.toUpperCase();
//            }
//        };


        System.out.println("The AnotherClass class's name is: " + getClass().getSimpleName());
        return Main.doStringStuff(uc,"String 1" , "String 2");


        //***   Without lambda expression
//        System.out.println("The another class's name is: " + getClass().getSimpleName());
//        return Main.doStringStuff(new UpperConcat() {
//            @Override
//            public String upperAndConcat(String s1, String s2) {
//                System.out.println("The anonymous class's name is: " + getClass().getSimpleName());
//                return s1.toUpperCase() + s2.toUpperCase();
//            };
//        }, "String 1" , "String 2");
    }
}