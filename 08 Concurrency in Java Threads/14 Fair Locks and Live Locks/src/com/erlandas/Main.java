package com.erlandas;

import java.util.concurrent.locks.ReentrantLock;

public class Main {
    /*
    LIVE LOCK:
        --  Live lock is similar to deadlock but instead of threads being locked they actually
            constantly active and usually waiting for all the other threads to complete their
            tasks. Since all the threads are waiting for all the others to complete, none of
            them can actually progress.

        --  Lets say Thread A will loop until Thread B completes his tasks
            And Thread B will loop until Thread A completes its tasks

        --  Thread A and B gets in to state when they looping and waiting for other Threads to complete.

        --  In this case Threads ar never progress but they ar not actually blocked


     */

    //Making fair lock TRUE meaning --> fair lock, we want to setup to be FIFO
    /*
        --  This way no more starvation
        --  Starvation when other threads waiting for turn and one thread taking all the time
     */
    private static ReentrantLock lock = new ReentrantLock(true);

    /**
     * Fair Locks try to be FIFO -> First In First Out
     *
     */

    public static void main(String[] args) {

        Thread t1 = new Thread(new Worker(ThreadColor.ANSI_RED),"Priority 10");
        Thread t2 = new Thread(new Worker(ThreadColor.ANSI_BLUE),"Priority 8");
        Thread t3 = new Thread(new Worker(ThreadColor.ANSI_GREEN),"Priority 6");
        Thread t4 = new Thread(new Worker(ThreadColor.ANSI_CYAN),"Priority 4");
        Thread t5 = new Thread(new Worker(ThreadColor.ANSI_PURPLE),"Priority 2");

        t1.setPriority(10);
        t2.setPriority(8);
        t3.setPriority(6);
        t4.setPriority(4);
        t5.setPriority(2);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();

    }

    private static class Worker implements Runnable {
        private int runCount = 1;
        private String threadColor;

        public Worker(String threadColor) {
            this.threadColor = threadColor;
        }

        public void run() {
            for (int i = 0 ; i < 100 ; i++) {
                lock.lock();
                try {
                    System.out.format(threadColor + "%s: runCount = %d\n", Thread.currentThread().getName(), runCount++);
                    //execute critical section of code
                } finally {
                    lock.unlock();
                }

            }
        }
    }
}
