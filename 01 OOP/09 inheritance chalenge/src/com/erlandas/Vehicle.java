package com.erlandas;

public class Vehicle {

    private double weight;
    private double wheelSize;
    private int wheelCount;

    public Vehicle(double weight, double wheelSize, int wheelCount) {
        this.weight = weight;
        this.wheelSize = wheelSize;
        this.wheelCount = wheelCount;
    }

    public void starts() {
        System.out.println(" is started");
    }
}
