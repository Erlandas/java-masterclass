package com.erlandas;


//this class already inheriting previous methods eat breathe from bird class
//we can owerride them if we want but not important


public class Parrot extends Bird {
    public Parrot(String name) {
        super(name);
    }

    @Override
    public void fly() {
        System.out.println(getName() + " flap flap flyes");
    }
}
