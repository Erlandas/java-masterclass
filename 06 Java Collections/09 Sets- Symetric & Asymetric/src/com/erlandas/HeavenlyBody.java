package com.erlandas;

import java.util.HashSet;
import java.util.Set;

public final class HeavenlyBody {

    private final String name;
    private final double orbitalPeriod;
    private final Set<HeavenlyBody> satellites;

    public HeavenlyBody(String name, double orbitalPeriod) {
        this.name = name;
        this.orbitalPeriod = orbitalPeriod;
        this.satellites = new HashSet<>();
    }

    //Getters
    public String getName() {
        return name;
    }

    public double getOrbitalPeriod() {
        return orbitalPeriod;
    }

    public Set<HeavenlyBody> getSatellites() {
        //Returns copy of satelites for security reasons
        return new HashSet<>(this.satellites);
    }

    //Add moon
    public boolean addMoon(HeavenlyBody moon) {
        return this.satellites.add(moon);
    }

    //Override equals
    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }

        System.out.println("obj.getClass is " +obj.getClass());
        System.out.println("this.getClass is " +this.getClass());
        if ((obj == null) || (obj.getClass() != this.getClass())) {
            return false;
        }

        String objName = ((HeavenlyBody) obj).getName();
        return this.name.equals(objName);
    }

    //Override hashCode
    @Override
    public int hashCode() {
        System.out.println("hashcode called " + name.hashCode());
        System.out.println("hashcode + 57 " + (name.hashCode()+57));
        return this.name.hashCode() + 57;
    }
}
