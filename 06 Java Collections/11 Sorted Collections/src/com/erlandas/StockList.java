package com.erlandas;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class StockList {
    private final Map<String, StockItem> list;

    public StockList() {
        this.list = new LinkedHashMap<>();
    }

    //Add stock
    public int addStock(StockItem item) {

        if(item != null) {
            //get the item if it already exists in a map if it doesn't exists its gone use this item
            //that we actually passed to this method
            StockItem inStock = list.getOrDefault(item.getName(), item);

            //if there already stocks of this item adjust the quantity
            if(inStock != item) {
                item.adjustStock(inStock.quantityInStock());
            }

            list.put(item.getName(), item);
            return item.quantityInStock();
        }
        return 0;
    }

    //Sell stock
    public int sellStock(String item, int quantity) {
        //Get if item exists if not get default as NULL
        StockItem inStock = list.getOrDefault(item , null);

        //if its not null && more then asks to sell in quantity and tries to but more then 0
        if((inStock != null) && (inStock.quantityInStock() >= quantity)  && (quantity > 0)) {
            //if passes all the test we deducting quantity
            inStock.adjustStock(-quantity);
            //return quantity to indicate how many items were taken of stock
            return quantity;
        }
        return 0;
    }

    //Getter for stock item
    public StockItem get(String key) {
        return list.get(key);
    }

    public Map<String, StockItem> Items() {
        //Provides unmodifiable collection read only access
        return Collections.unmodifiableMap(list);
    }

    public Map<String, Double> priceList() {
        Map<String, Double> prices = new LinkedHashMap<>();
        for (Map.Entry<String, StockItem> item : list.entrySet()) {
            prices.put(item.getKey(), item.getValue().getPrice());
        }
        return Collections.unmodifiableMap(prices);
    }

    //override toString()
    @Override
    public String toString() {
        String s = "\nStock List\n";
        double totalCost = 0.0;
        for(Map.Entry<String, StockItem> item: list.entrySet() ) {
            StockItem stockItem = item.getValue();

            double itemValue = stockItem.getPrice() * stockItem.quantityInStock();

            s = s + stockItem + ". There are " + stockItem.quantityInStock() + " in stock. Value of items: ";
            s = s + String.format("%.2f",itemValue) + "\n";

            totalCost += itemValue;
        }

        return s + "Total stock value " + totalCost;
    }
}
