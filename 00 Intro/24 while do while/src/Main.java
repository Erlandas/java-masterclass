public class Main {

    public static void main(String[] args) {

//        int number = 4;
//        int finishNumber = 20;
//
//        while(number <= finishNumber) {
//            number++;
//            if(!isEvenNumber(number)) {
//                continue;
//            }
//
//            System.out.println("Even number " + number);
//        }

        int num = 10;
        int count = 0;

        while (true) {
            num++;
            if(!isEvenNumber(num)){
                continue;
            }
            count++;
            System.out.println("Even Number " + num);
            if(count == 5) break;
        }

        System.out.println("\nEven numbers found " + count);
    }

    public static boolean isEvenNumber(int number) {
        if(number % 2 == 0) {
            return true;
        }

        return false;
    }
}
