package com.erlandas;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        Employee john = new Employee("John Doe" , 30);
        Employee jane = new Employee("Jane Deer" , 25);
        Employee jack = new Employee("Jack Hill" , 40);
        Employee snow = new Employee("Snow White" , 22);

        Department hr = new Department("Human Resources");
        hr.addEmployee(jane);
        hr.addEmployee(jack);
        hr.addEmployee(snow);

        Department accounting = new Department("Accounting");
        accounting.addEmployee(john);

        //--    Lets we want to print all the employees that works for the company
        //      1. Create a list of department objects
        List<Department> departments = new ArrayList<>();
        departments.add(hr);
        departments.add(accounting);

        //      2. using stream to print all the employees
        departments.stream()
                //--    flatMap -> once a function that returns a stream
                //--    each department in the source stream becomes an argument to the function
                //--    for each department we call getEmployees() method
                //--    which returns a list and the we call stream() method
                //--    on that list to return a stream of employees
                //--    now the items from a stream were added to the stream
                //--    will be returned from the flatMap method
                //--
                //--    flatMap is used when we want to perform operations on the list
                //--    but list isn't the source, in this scenario object
                //--    containing a list is the source
                //--
                //--    we use the method to create a stream of all the objects
                //--    in those lists
                .flatMap(department -> department.getEmployees().stream())
                .forEach(System.out::println);

        //--    Group by age
        Map<Integer, List<Employee>> groupedByAge = departments.stream()
                .flatMap(department -> department.getEmployees().stream())
                .collect(Collectors.groupingBy(employee -> employee.getAge()));

        System.out.println("------------------Youngest Employee---------------------");
        //--    Reduces a stream to one of elements in a stream
        //--    to find youngest employee in the company
        departments.stream()
                .flatMap(department -> department.getEmployees().stream())
                .reduce((e1,e2) -> e1.getAge() < e2.getAge() ? e1 : e2)
                .ifPresent(System.out::println);

        System.out.println("------------------Filter Predicates---------------------");
        //--    Stream using predicates in filter
        Stream.of("ABC","AC","BAA","CCCC","XY","ST")
                .filter(s -> {
                    //--    Nothing is printed
                    //--    Nothing happens until there is a terminal operation
                    System.out.println(s);
                    return s.length() == 3;
                })
                .count();


    }

}
