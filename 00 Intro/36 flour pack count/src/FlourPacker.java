public class FlourPacker {

    public static boolean canPack(int bigCount, int smallCount, int goal) {
        //5kg 1kg

        if(bigCount < 0 || smallCount < 0 || goal < 0) return false;

        int bc = bigCount * 5; //5
        int sc = smallCount; //0

        //5>=5                5%5 <= 0
        if(bc >= goal) return goal%5<=smallCount;

               //5+0 >= 5 and 1<=5
        return bc+sc>=goal && bigCount<=goal;
    }
}
